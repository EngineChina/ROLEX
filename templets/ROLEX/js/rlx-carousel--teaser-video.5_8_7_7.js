define("modules/rlx-carousel--teaser-video", [], function(){ "use strict"
  var $ = jQuery
  var jBody = $('body')
  var isMobile = RLX.GLOBALS.features.isMobile
  var $main = []
  var dataNode=null
  var headerH = 50
  var videosample=null
  var vcol=[]
  var vactive=null
  var interacted=false
  var played=[]
  var cindex = -1
  var uA = window.navigator.userAgent
  var onlyIEorEdge = /msie\s|trident\/|edge\//i.test(uA) && !!( document.uniqueID || window.MSInputMethodContext)
  var onlyIE = onlyIEorEdge && !/edge\//i.test(uA)
//  var vccCol = []
//  var reqint = 0
  var _REQINTDURATION = 400
  var _DUR = 0
  var _DELAY = 0
  var _ATM = 0
  var _IOSV = 0
  var _LOOP = false
  var initialized = {}
  var restm = 0
  var iecn = 'rlx-teaser--sliding'
  var $marker = []
//  var loadingcn = 'rlx-teaser--loading'
/*
	function trackVideo() {
		this.addEventListener('abort',function(e){console.log(e.type,this)})
		this.addEventListener('canplay',function(e){console.log(e.type,this)})
		this.addEventListener('canplaythrough',function(e){console.log(e.type,this)})
		this.addEventListener('durationchange',function(e){console.log(e.type,this)})
		this.addEventListener('emptied',function(e){console.log(e.type,this)})
		this.addEventListener('encrypted',function(e){console.log(e.type,this)})
		this.addEventListener('ended',function(e){console.log(e.type,this)})
		this.addEventListener('error',function(e){console.log(e.type,this)})
		this.addEventListener('interruptbegin',function(e){console.log(e.type,this)})
		this.addEventListener('interruptend',function(e){console.log(e.type,this)})
		this.addEventListener('loadeddata',function(e){console.log(e.type,this)})
		this.addEventListener('loadedmetadata',function(e){console.log(e.type,this)})
		this.addEventListener('loadstart',function(e){console.log(e.type,this)})
		this.addEventListener('pause',function(e){console.log(e.type,this)})
		this.addEventListener('play',function(e){console.log(e.type,this)})
		this.addEventListener('playing',function(e){console.log(e.type,this)})
		this.addEventListener('progress',function(e){console.log(e.type,this)})
		this.addEventListener('ratechange',function(e){console.log(e.type,this)})
		this.addEventListener('seeked',function(e){console.log(e.type,this)})
		this.addEventListener('seeking',function(e){console.log(e.type,this)})
		this.addEventListener('stalled',function(e){console.log(e.type,this)})
		this.addEventListener('suspend',function(e){console.log(e.type,this)})
		this.addEventListener('timeupdate',function(e){console.log(e.type,this)})
		this.addEventListener('volumechange',function(e){console.log(e.type,this)})
		this.addEventListener('waiting',function(e){console.log(e.type,this)})
	}
*/
	function iOSversion() {
	  if (/iP(hone|od|ad)/.test(navigator.platform)) {
		// supports iOS 2.0 and later: <http://bit.ly/TJjs1V>
		var v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
		return parseInt(v[1], 10);
	  }
	}

	function testVideoPlay() {
//		console.log('-- carousel teaser video testVideoPlay')
		var dfd = $.Deferred()

		if (
			(RLX.GLOBALS.features.video_policy.client_speed == RLX.GLOBALS.features.video_policy.SPEED_HIGH) ||
			(RLX.GLOBALS.features.video_policy.client_speed == RLX.GLOBALS.features.video_policy.SPEED_MID)
		) {
			if (onlyIEorEdge) {
				initVideoPlaying.call(dfd, true)
			} else {
				videosample = document.createElement('video')
				videosample.muted=true
				videosample.setAttribute('playsinline', 'true')
				videosample.setAttribute('src', 'data:video/mp4;base64, AAAAHGZ0eXBNNFYgAAACAGlzb21pc28yYXZjMQAAAAhmcmVlAAAGF21kYXTeBAAAbGliZmFhYyAxLjI4AABCAJMgBDIARwAAArEGBf//rdxF6b3m2Ui3lizYINkj7u94MjY0IC0gY29yZSAxNDIgcjIgOTU2YzhkOCAtIEguMjY0L01QRUctNCBBVkMgY29kZWMgLSBDb3B5bGVmdCAyMDAzLTIwMTQgLSBodHRwOi8vd3d3LnZpZGVvbGFuLm9yZy94MjY0Lmh0bWwgLSBvcHRpb25zOiBjYWJhYz0wIHJlZj0zIGRlYmxvY2s9MTowOjAgYW5hbHlzZT0weDE6MHgxMTEgbWU9aGV4IHN1Ym1lPTcgcHN5PTEgcHN5X3JkPTEuMDA6MC4wMCBtaXhlZF9yZWY9MSBtZV9yYW5nZT0xNiBjaHJvbWFfbWU9MSB0cmVsbGlzPTEgOHg4ZGN0PTAgY3FtPTAgZGVhZHpvbmU9MjEsMTEgZmFzdF9wc2tpcD0xIGNocm9tYV9xcF9vZmZzZXQ9LTIgdGhyZWFkcz02IGxvb2thaGVhZF90aHJlYWRzPTEgc2xpY2VkX3RocmVhZHM9MCBucj0wIGRlY2ltYXRlPTEgaW50ZXJsYWNlZD0wIGJsdXJheV9jb21wYXQ9MCBjb25zdHJhaW5lZF9pbnRyYT0wIGJmcmFtZXM9MCB3ZWlnaHRwPTAga2V5aW50PTI1MCBrZXlpbnRfbWluPTI1IHNjZW5lY3V0PTQwIGludHJhX3JlZnJlc2g9MCByY19sb29rYWhlYWQ9NDAgcmM9Y3JmIG1idHJlZT0xIGNyZj0yMy4wIHFjb21wPTAuNjAgcXBtaW49MCBxcG1heD02OSBxcHN0ZXA9NCB2YnZfbWF4cmF0ZT03NjggdmJ2X2J1ZnNpemU9MzAwMCBjcmZfbWF4PTAuMCBuYWxfaHJkPW5vbmUgZmlsbGVyPTAgaXBfcmF0aW89MS40MCBhcT0xOjEuMDAAgAAAAFZliIQL8mKAAKvMnJycnJycnJycnXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXiEASZACGQAjgCEASZACGQAjgAAAAAdBmjgX4GSAIQBJkAIZACOAAAAAB0GaVAX4GSAhAEmQAhkAI4AhAEmQAhkAI4AAAAAGQZpgL8DJIQBJkAIZACOAIQBJkAIZACOAAAAABkGagC/AySEASZACGQAjgAAAAAZBmqAvwMkhAEmQAhkAI4AhAEmQAhkAI4AAAAAGQZrAL8DJIQBJkAIZACOAAAAABkGa4C/AySEASZACGQAjgCEASZACGQAjgAAAAAZBmwAvwMkhAEmQAhkAI4AAAAAGQZsgL8DJIQBJkAIZACOAIQBJkAIZACOAAAAABkGbQC/AySEASZACGQAjgCEASZACGQAjgAAAAAZBm2AvwMkhAEmQAhkAI4AAAAAGQZuAL8DJIQBJkAIZACOAIQBJkAIZACOAAAAABkGboC/AySEASZACGQAjgAAAAAZBm8AvwMkhAEmQAhkAI4AhAEmQAhkAI4AAAAAGQZvgL8DJIQBJkAIZACOAAAAABkGaAC/AySEASZACGQAjgCEASZACGQAjgAAAAAZBmiAvwMkhAEmQAhkAI4AhAEmQAhkAI4AAAAAGQZpAL8DJIQBJkAIZACOAAAAABkGaYC/AySEASZACGQAjgCEASZACGQAjgAAAAAZBmoAvwMkhAEmQAhkAI4AAAAAGQZqgL8DJIQBJkAIZACOAIQBJkAIZACOAAAAABkGawC/AySEASZACGQAjgAAAAAZBmuAvwMkhAEmQAhkAI4AhAEmQAhkAI4AAAAAGQZsAL8DJIQBJkAIZACOAAAAABkGbIC/AySEASZACGQAjgCEASZACGQAjgAAAAAZBm0AvwMkhAEmQAhkAI4AhAEmQAhkAI4AAAAAGQZtgL8DJIQBJkAIZACOAAAAABkGbgCvAySEASZACGQAjgCEASZACGQAjgAAAAAZBm6AnwMkhAEmQAhkAI4AhAEmQAhkAI4AhAEmQAhkAI4AhAEmQAhkAI4AAAAhubW9vdgAAAGxtdmhkAAAAAAAAAAAAAAAAAAAD6AAABDcAAQAAAQAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwAAAzB0cmFrAAAAXHRraGQAAAADAAAAAAAAAAAAAAABAAAAAAAAA+kAAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAABAAAAAALAAAACQAAAAAAAkZWR0cwAAABxlbHN0AAAAAAAAAAEAAAPpAAAAAAABAAAAAAKobWRpYQAAACBtZGhkAAAAAAAAAAAAAAAAAAB1MAAAdU5VxAAAAAAALWhkbHIAAAAAAAAAAHZpZGUAAAAAAAAAAAAAAABWaWRlb0hhbmRsZXIAAAACU21pbmYAAAAUdm1oZAAAAAEAAAAAAAAAAAAAACRkaW5mAAAAHGRyZWYAAAAAAAAAAQAAAAx1cmwgAAAAAQAAAhNzdGJsAAAAr3N0c2QAAAAAAAAAAQAAAJ9hdmMxAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAALAAkABIAAAASAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGP//AAAALWF2Y0MBQsAN/+EAFWdCwA3ZAsTsBEAAAPpAADqYA8UKkgEABWjLg8sgAAAAHHV1aWRraEDyXyRPxbo5pRvPAyPzAAAAAAAAABhzdHRzAAAAAAAAAAEAAAAeAAAD6QAAABRzdHNzAAAAAAAAAAEAAAABAAAAHHN0c2MAAAAAAAAAAQAAAAEAAAABAAAAAQAAAIxzdHN6AAAAAAAAAAAAAAAeAAADDwAAAAsAAAALAAAACgAAAAoAAAAKAAAACgAAAAoAAAAKAAAACgAAAAoAAAAKAAAACgAAAAoAAAAKAAAACgAAAAoAAAAKAAAACgAAAAoAAAAKAAAACgAAAAoAAAAKAAAACgAAAAoAAAAKAAAACgAAAAoAAAAKAAAAiHN0Y28AAAAAAAAAHgAAAEYAAANnAAADewAAA5gAAAO0AAADxwAAA+MAAAP2AAAEEgAABCUAAARBAAAEXQAABHAAAASMAAAEnwAABLsAAATOAAAE6gAABQYAAAUZAAAFNQAABUgAAAVkAAAFdwAABZMAAAWmAAAFwgAABd4AAAXxAAAGDQAABGh0cmFrAAAAXHRraGQAAAADAAAAAAAAAAAAAAACAAAAAAAABDcAAAAAAAAAAAAAAAEBAAAAAAEAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAkZWR0cwAAABxlbHN0AAAAAAAAAAEAAAQkAAADcAABAAAAAAPgbWRpYQAAACBtZGhkAAAAAAAAAAAAAAAAAAC7gAAAykBVxAAAAAAALWhkbHIAAAAAAAAAAHNvdW4AAAAAAAAAAAAAAABTb3VuZEhhbmRsZXIAAAADi21pbmYAAAAQc21oZAAAAAAAAAAAAAAAJGRpbmYAAAAcZHJlZgAAAAAAAAABAAAADHVybCAAAAABAAADT3N0YmwAAABnc3RzZAAAAAAAAAABAAAAV21wNGEAAAAAAAAAAQAAAAAAAAAAAAIAEAAAAAC7gAAAAAAAM2VzZHMAAAAAA4CAgCIAAgAEgICAFEAVBbjYAAu4AAAADcoFgICAAhGQBoCAgAECAAAAIHN0dHMAAAAAAAAAAgAAADIAAAQAAAAAAQAAAkAAAAFUc3RzYwAAAAAAAAAbAAAAAQAAAAEAAAABAAAAAgAAAAIAAAABAAAAAwAAAAEAAAABAAAABAAAAAIAAAABAAAABgAAAAEAAAABAAAABwAAAAIAAAABAAAACAAAAAEAAAABAAAACQAAAAIAAAABAAAACgAAAAEAAAABAAAACwAAAAIAAAABAAAADQAAAAEAAAABAAAADgAAAAIAAAABAAAADwAAAAEAAAABAAAAEAAAAAIAAAABAAAAEQAAAAEAAAABAAAAEgAAAAIAAAABAAAAFAAAAAEAAAABAAAAFQAAAAIAAAABAAAAFgAAAAEAAAABAAAAFwAAAAIAAAABAAAAGAAAAAEAAAABAAAAGQAAAAIAAAABAAAAGgAAAAEAAAABAAAAGwAAAAIAAAABAAAAHQAAAAEAAAABAAAAHgAAAAIAAAABAAAAHwAAAAQAAAABAAAA4HN0c3oAAAAAAAAAAAAAADMAAAAaAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAACMc3RjbwAAAAAAAAAfAAAALAAAA1UAAANyAAADhgAAA6IAAAO+AAAD0QAAA+0AAAQAAAAEHAAABC8AAARLAAAEZwAABHoAAASWAAAEqQAABMUAAATYAAAE9AAABRAAAAUjAAAFPwAABVIAAAVuAAAFgQAABZ0AAAWwAAAFzAAABegAAAX7AAAGFwAAAGJ1ZHRhAAAAWm1ldGEAAAAAAAAAIWhkbHIAAAAAAAAAAG1kaXJhcHBsAAAAAAAAAAAAAAAALWlsc3QAAAAlqXRvbwAAAB1kYXRhAAAAAQAAAABMYXZmNTUuMzMuMTAw')
				var samplePromise = videosample.play()
				if (samplePromise !== undefined) {
					samplePromise.then(initVideoPlaying.bind(dfd, true), initVideoPlaying.bind(dfd, false))
				} else {
					initVideoPlaying.call(dfd, false)
				}
			}
		} else {
		  // the video policy indicates that we have insufficient bandwidth for videos
		  // so we reject the promise outright
			dfd.reject()
		}

		return dfd
	}

	function keepCacheVideoAlive(src) {
//		console.log('-- model--cover-install keepCacheVideoAlive')
		function onWaiting(e) {
			if (this.currentTime===0||this.currentTime>=this.duration)
				return
//			console.log('-- model--cover-install keepCacheVideoAlive onWaiting',this.currentTime)
			this.currentTime=this.currentTime
//			setTimeout(this.play.bind(this),4)
		}

		function onPlay(e) {
//			console.log('-- model--cover-install keepCacheVideoAlive onPlay')
			this.removeEventListener('waiting', onWaiting, false)
			this.addEventListener('waiting', onWaiting, false)
		}

		function onPause(e) {
//			console.log('-- model--cover-install keepCacheVideoAlive onPause')
			this.removeEventListener('waiting', onWaiting, false)
		}

		function onLoadStart(e) {
//			console.log('-- model--cover-install keepCacheVideoAlive onLoadStart')
			this.removeEventListener('loadstart', onLoadStart, false)
			dfd.resolve()
		}

		var dfd = $.Deferred()
//		trackVideo.call(this)
		this.removeEventListener('loadstart', onLoadStart, false)
		this.addEventListener('loadstart', onLoadStart, false)
		this.src=src
		this.load()
		this.removeEventListener('play', onPlay, false)
		this.addEventListener('play', onPlay, false)
		this.removeEventListener('pause', onPause, false)
		this.addEventListener('pause', onPause, false)

		return dfd
	}

	function initVideoPlaying(state) {
//		console.log('-- carousel teaser video initVideoPlaying',state)
		videosample=null
		if (state)
			this.resolve()
		else
			this.reject()
	}

	function preloadVideo() {
//		console.log('-- preloadVideo', this)
//		vactive=vcol[this.index]
		if (this.loading)
			return
		if (this.ready)
			return startVideo.call(this)
		return loadVideo.call(this,this.video.src).then(onLoadVideoSuccess.bind(this),onLoadVideoError.bind(this))
	}

	function createBlob(data, datatype) {
//		console.log('-- createBlob', datatype)
		var blob
		try {
			blob = new Blob([data], {type: datatype})
		} catch (e) {
			if (e.name == "InvalidStateError") {
				var bb = new BlobBuilder()
				bb.append(data)
				blob = bb.getBlob(datatype)
			} else {
				blob = null
			}
		}
		return blob
	}

	function loadVideo(src) {
//		console.log('-- loadVideo', this, src)
		var updateProgress = function(e) {
			this.progress = e.lengthComputable?e.loaded/e.total:0
		}.bind(this)

		var request = new XMLHttpRequest()
		var dfd = $.Deferred()
		if (!request)
			dfd.reject()
		else {
			request.open('GET', src)
			request.timeout = 0
			request.addEventListener('progress', updateProgress, false)
			request.responseType = 'blob'
			request.onload = function() {
				if (request.status===200||request.status===206) {
					var blob = createBlob(request.response, 'video/mp4')
					dfd.resolve(!!blob?URL.createObjectURL(blob):src)
				}
				else {
					dfd.reject()
				}
			}.bind(this)
			request.send()
			this.vloadstart = new Date().getTime()
			this.loading=true
		}
		return dfd
	}

	function onLoadVideoSuccess(src) {
//		console.log('-- onLoadVideoSuccess', this)
		keepCacheVideoAlive.call(this.video,src).then(this.reset.bind(this,true))
	}

	function onLoadVideoError(sindex) {
//		console.log('-- onLoadVideoError',sindex)
	}

	function hidePoster() {
//		console.log('-- hidePoster',this.poster)
		TweenMax.set(this.poster, {autoAlpha: 0,})
		this.fade=-1
	}

	function fadeVideo() {
		if (this.fade<0)
			return
//		console.log('-- fadeVideo',this)
		if (this.fade>0)
			TweenMax.to(this.comp, this.fade, {autoAlpha: 1, ease: Power4.easeOut, onComplete: hidePoster, onCompleteScope: this,})
		else if (this.fade===0)
			hidePoster.call(this)
		this.fade=-1
	}

	function startVideo() {
//		console.log('-- startVideo',this.vloadstart)
		dataNode.pauseAutoplay(true)

		if (!!this.vloadstart) {
			var dl = this.delay-new Date().getTime()+this.vloadstart
			dl=dl>0?dl:0
			this.vloadstart=0
			return setTimeout(startVideo.bind(this), dl)
		}
		if(!isCarouselVisible()||RLX.accessibility.stopAnimations)
			return

		playVideo.call(this).then(fadeVideo.bind(this))
	}

	function printPoster(src,dfd) {
//		console.log('-- printPoster',src)
		if (!!this.poster)
			return dfd.resolve()
		var $asset = $(this.el).find('.rlx-carousel--teaser-link')
		var im = document.createElement('img')
		im.src = src
		im.className = 'rlx-carousel--teaser-poster'
		this.poster=im
		$asset.prepend($(im))
		dfd.resolve()
	}

	function printTVPoster(leave) {
//		console.log('-- printTVPoster',vactive)
  		var dfd = $.Deferred()
  		if (leave && onlyIE)
  			dfd.resolve()
  		else if (!vactive||!vactive.ready||!vactive.poster||!vactive.poster.snap)
  			dfd.resolve()
  		else {
//			pauseVideo.call(vactive)
			printPoster.call(vactive,dfd)
		}
  		return dfd
  	}

	function preloadPoster(src) {
//		console.log('-- preloadPoster',src)
		var request = new XMLHttpRequest()
		var dfd = $.Deferred()
		request.open('GET', src)
		request.responseType = 'blob'
		request.onload = function() {
			if (this.status===200) {
				var blob = createBlob(this.response, 'image/jpeg')
				dfd.resolve(!!blob?URL.createObjectURL(blob):src)
			}
			else {
				dfd.reject()
			}
		}
		request.send()
		return dfd
	}

	// init video element
	function initVideo(vPolicy,dfd) {
//		console.log('-- initVideo', vPolicy,dfd)
		function onPosterSuccess(src) {
//			console.log('-- onPosterSuccess',src)
			this.video.removeAttribute('poster')
			printPoster.call(this,src,dfd)
		}

		function onPosterError() {
//			console.log('-- onPosterError')
			dfd.resolve()
		}
/*
		if (this.index===current)
			dfd.then(preloadVideo.bind(this))
*/
		if (vPolicy) {
			vcol.push(this)
			this.ready = false
			this.video.muted = true
			this.video.setAttribute('playsinline', 'true')
			this.video.onerror=function(){console.log(this.error.code)}
			var $item = $(this.el).find('.rlx-carousel--teaser-item')
			this.fade = ($item.data('slide-video-fade')||0)/1000
			this.video.addEventListener('ended', endVideo.bind(this), false)
			this.video.addEventListener('seeked', preloadVideo.bind(this), false)
			this.track = preloadVideo
			if (!!this.fade)
				TweenMax.set(this.comp, {autoAlpha: 0,})
		}
		preloadPoster(this.video.getAttribute('poster')).then(onPosterSuccess.bind(this), onPosterError.bind(this))
	}

	function endVideo() {
//		console.log('-- endVideo',this)
		var single=$main.find('.rlx-carousel-slide').length<=1
		if (!_LOOP)
			this.track=null
		if (interacted||(single&&_LOOP)) {
			this.video.currentTime=0
//			preloadVideo.call(this)
		}
		else if (!single) {
			dataNode.pauseAutoplay(false)
			dataNode.goNext()
		}
	}

	function playVideo() {
//		console.log('-- playVideo',this)
		var o = this

		var dfd = $.Deferred()
		play.call(o.video)

		return dfd

		function play() {
//			console.log('-- playVideo play',this)
			this.removeEventListener('timeupdate', onTimeupdate, false)
			this.addEventListener('timeupdate', onTimeupdate, false)
			this.play()
		}

		function onTimeupdate() {
//			console.log('-- playVideo onTimeupdate',this)
			this.removeEventListener('timeupdate', onTimeupdate, false)
			dfd.resolve()
		}
	}

	function isCarouselVisible() {
//		console.log('-- isCarouselVisible')
		var scroll = window.pageYOffset || window.scrollY || document.documentElement.scrollTop || document.body.scrollTop || 0
		return (scroll<$main.offset().top+$main.outerHeight()-headerH)
	}

  return {
  	init: function (main, data) {
//  		console.log('-- carousel teaser video init')
  		$main = main
  		dataNode = data
//  			dataNode.forceFreeze()
			if (onlyIEorEdge)
				$main.addClass('rlx-carousel--teaser-ms')

			if (onlyIE)
				$main.addClass('rlx-carousel--teaser-msie')/*,
				dataNode.forceFreeze()*/

			_IOSV = iOSversion()||0
			_DUR = +$main.data('rlx-carousel-duration')||0
			_DELAY = $main.data('teaser-video-delay')||0
			_ATM = $main.data('rlx-carousel-delay')||0
			_LOOP = $main.data('rlx-carousel-loop')
			window.BlobBuilder = window.BlobBuilder ||
			window.WebKitBlobBuilder ||
			window.MozBlobBuilder ||
			window.MSBlobBuilder

			return testVideoPlay()
  	},
  	launch: function(tvReady) {
//  		console.log('-- carousel teaser video launch')
		$main.on(window.rlxCarousel.events.update, browseVideos.bind(tvReady))
		if (onlyIE)
			$main.on(window.rlxCarousel.events.indexChange, onIEIndexChange),
			$main.on(window.rlxCarousel.events.afterIndexChange, onAfterIndexChange)
		else
			$main.on(window.rlxCarousel.events.indexChange, onIndexChange)
		initMobileContext(!!_IOSV)
		browseVideos.call(tvReady)
  	},
/*  	onScroll: function (state) {
  		if (!vactive)
			return
		if (state)
			preloadVideo.call(vactive)
		else
			pauseVideo.call(vactive)
  	},*/
  	setInteracted: function (state) {
  		if (interacted === state)
  			return
//  		console.log('-- setInteracted',state)
  		interacted = state
////		if (!!vactive)
////			updateVideoLoop.call(vactive.video,interacted)
  	},
  	printPoster: printTVPoster,
  	initVideo: initVideo,
  	isFocused: isCarouselVisible,
  }
});
