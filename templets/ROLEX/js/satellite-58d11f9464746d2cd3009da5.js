//Ajout de la variable PageTemplate dans le DDL 
_satellite.getDataElement('DL - PageTemplate CS');

_satellite.getVar("DL - COMP - Full path");

(function () {
    window._uxa = window._uxa || [];
    
    var category = "";
    var id = "";
    var price = "";
    //try {
        if (typeof digitalDataLayer !== 'undefined' && typeof digitalDataLayer.page !== 'undefined' && typeof digitalDataLayer.page.responseCode !== 'undefined')
            window._uxa.push(['setCustomVariable', 1, 'responsecode', digitalDataLayer.page.responseCode, 3]);
        if (typeof digitalDataLayer !== 'undefined' && typeof digitalDataLayer.attributes !== 'undefined' && typeof digitalDataLayer.attributes.countryCode !== 'undefined')
            window._uxa.push(['setCustomVariable', 2, 'countryCode', digitalDataLayer.attributes.countryCode, 3]);
        if (typeof digitalDataLayer !== 'undefined' && typeof digitalDataLayer.page !== 'undefined' && typeof digitalDataLayer.page.products !== 'undefined'){
            if(typeof digitalDataLayer.page.products.selections !== "undefined"){
                digitalDataLayer.page.products.selections.forEach(function(product) {
                    category = category != "" ? category + "|" : category;
                    category = category + product.category;
                    id = id != "" ? id + "|" : id;
                    id = id + product.id;
                    price = price != "" ? price + "|" : price;
                    price = price + product.price;
                    window._uxa.push(['setCustomVariable', 3, 'category', category, 3]);
                    window._uxa.push(['setCustomVariable', 4, 'id', id, 3]);
                    window._uxa.push(['setCustomVariable', 5, 'price', price, 3]);
                });
            }else{
	            if(typeof digitalDataLayer.page.products.detail !== "undefined"){
                    if (typeof digitalDataLayer.page.products.detail[0].category !== 'undefined') window._uxa.push(['setCustomVariable', 3, 'category', digitalDataLayer.page.products.detail[0].category, 3]);
                    if (typeof digitalDataLayer.page.products.detail[0].id !== 'undefined')       window._uxa.push(['setCustomVariable', 4, 'id', digitalDataLayer.page.products.detail[0].id, 3]);
                    if (typeof digitalDataLayer.page.products.detail[0].price !== 'undefined')    window._uxa.push(['setCustomVariable', 5, 'price',digitalDataLayer.page.products.detail[0].price, 3]);
                }
            }
        }
        if (typeof digitalDataLayer !== 'undefined' && typeof digitalDataLayer.attributes !== 'undefined' && typeof digitalDataLayer.attributes.countryZone !== 'undefined')
            window._uxa.push(['setCustomVariable', 6, 'countryZone', digitalDataLayer.attributes.countryZone, 3]);
        if (typeof digitalDataLayer !== 'undefined' && typeof digitalDataLayer.attributes !== 'undefined' && typeof digitalDataLayer.attributes.currency !== 'undefined')
            window._uxa.push(['setCustomVariable', 7, 'currency', digitalDataLayer.attributes.currency, 3]);
  			if (typeof digitalDataLayer !== 'undefined' && typeof digitalDataLayer.page !== 'undefined')
            window._uxa.push(['setCustomVariable', 8, 'pageTemplate',digitalDataLayer.page.pageTemplate, 3]);
  
    //} catch(e){}
    if (typeof CS_CONF === 'undefined') {
        window._uxa.push(['setPath', window.location.pathname+window.location.hash.replace('#','?__')]);
        var mt = document.createElement("script"); mt.type = "text/javascript"; mt.async = true;
        mt.src = "https://static.rolex.com/cs_rolex_trk/cs_rolex_prod.js";
        document.getElementsByTagName("head")[0].appendChild(mt);
    }
    else {
        window._uxa.push(['trackPageview', _satellite.getVar("DL - COMP - Full path").replace('#','?__')]);
     // window._uxa.push(['trackPageview', window.location.pathname+window.location.hash.replace('#','?__')]);
      
    }
})();