window.onload = function () {
    // Swiper 1 劳力士腕表
    var mySwiper1 = new Swiper('.kdSwiperContainer1', {
        spaceBetween: 10,
        slidesPerView: 'auto',
        freeMode: true,

        loop: false, // 循环模式选项
        // 如果需要前进后退按钮
        navigation: {
            nextEl: '.kdBtnNext1',
            prevEl: '.kdBtnPrev1',
        },
        // 如果需要滚动条
        scrollbar: {
            el: '.kdScrollbar1',
        },
    })
    // swiper 2 2019年新款腕表
    var mySwiper2 = new Swiper('.kdSwiperContainer2', {
        spaceBetween: 10,
        slidesPerView: 'auto',
        freeMode: true,
        loop: false, // 循环模式选项
        // 如果需要前进后退按钮
        navigation: {
            nextEl: '.kdBtnNext2',
            prevEl: '.kdBtnPrev2',
        },
        // 如果需要滚动条
        scrollbar: {
            el: '.kdScrollbar2',
        },
    })
    // swiper 3 探索劳力士
    var mySwiper3 = new Swiper('.kdSwiperContainer3', {
        spaceBetween: 10,
        slidesPerView: 'auto',
        freeMode: true,
        loop: false, // 循环模式选项
        // 如果需要前进后退按钮
        navigation: {
            nextEl: '.kdBtnNext3',
            prevEl: '.kdBtnPrev3',
        },
        // 如果需要滚动条
        scrollbar: {
            el: '.kdScrollbar3',
        },
    })
    // swiper 5 文章内容更多列表
    var mySwiper5 = new Swiper('.kdSwiperContainer5', {
        spaceBetween: 10,
        slidesPerView: 'auto',
        freeMode: true,
        loop: false, // 循环模式选项
        // 如果需要前进后退按钮
        navigation: {
            nextEl: '.kdBtnNext5',
            prevEl: '.kdBtnPrev5',
        },
        // 如果需要滚动条
        scrollbar: {
            el: '.kdScrollbar5',
            dragSize: 30,
        },
    })
    // swiper 4 menu 图文列表
    var mySwiper4 = new Swiper('.kdSwiperContainer4', {
        spaceBetween: 10,
        slidesPerView: 'auto',
        freeMode: true,
        loop: false, // 循环模式选项
        // 如果需要滚动条
        scrollbar: {
            el: '.kdScrollbar4',
            dragSize: 30,
        },
    })
    mySwiper4.scrollbar.$el.css('color', '#fff');
}