define("cq-modules/rlx-members-only", ["config-members", "modules/rmc", "cq-modules/rlx-cookie-utils"],
    function (config, RMC, cookieUtils) {
        var $ = window.jQuery,
            DEFAULT_WISH_LIST = "Personal",
            DEFAULT_ORDER = "0";


        function logError(message) {
            console.log(message);
        }

        function logSuccess(message) {
            console.log(message);
        }

        function isLogged() {
            return getMemberData() && getMemberData().id;
        }


        function getMemberData() {
            var data = cookieUtils.atobUtf8(($.cookie(config.memberData) || "")).split(";");
            if (data && data.length) {
                return {"id": data[0]}
            }
            return {};
        }


        /**
         * Wishlist handling functions
         */

        function wishListContains(wishLists, wishlistId, rmc) {
            if (wishlistId && rmc) {
                if (wishLists && wishLists.length) {
                    for (var i = 0, len = wishLists.length; i < len; i++) {
                        if ((wishLists[i].id == wishlistId) && wishLists[i].items && wishLists[i].items.length) {
                            for (var j = 0; j < wishLists[i].items.length; j++) {
                                if (RMC.urlescape(wishLists[i].items[j].productId.toUpperCase()) == RMC.urlescape(rmc.toUpperCase())) {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }

        function addToDefaultWishList(rmc, nb) {
            var dfd = new $.Deferred(),
                dwl;
            loadWishLists()
                .done(function (wishLists) {
                    if (wishLists && Array.isArray(wishLists) && nb && wishLists.length < nb) {
                        dwl = defaultWishlist(wishLists);
                        if (dwl && !wishListContains(wishLists, dwl.id, rmc)) {
                            addToWishList(dwl.id, rmc).done(function (data) {
                                dfd.resolve(data);
                            });
                        }
                    } else {
                        dfd.resolve(false);
                    }
                });
            return dfd;
        }

        function removeFromDefaultWishList(rmc) {
            var dfd = new $.Deferred(),
                dwl;
            loadWishLists()
                .done(function (wishLists) {
                    if (wishLists && wishLists.length) {
                        dwl = defaultWishlist(wishLists);
                        if (dwl && wishListContains(wishLists, dwl.id, rmc)) {
                            deleteFromWishList(dwl.id, rmc).done(function (data) {
                                dfd.resolve(data);
                            });
                        }
                    } else {
                        dfd.resolve(false);
                    }
                });
            return dfd;
        }


        function defaultWishlist(wishLists) {
            if (wishLists) {
                for (var i = 0, len = wishLists.length; i < len; i++) {
                    if (wishLists[i].name == DEFAULT_WISH_LIST) {
                        return wishLists[i];
                    }
                }
            }
        }

        function addToWishList(wishlistId, rmc) {
            var dfd = new $.Deferred();
            if (isLogged() && wishlistId && rmc) {
                ajaxPost(
                    {
                        "cmd": "addToWishList",
                        "memberId": getMemberData().id,
                        "wishlistId": wishlistId,
                        "rmc": rmc
                    }
                ).done(
                    function (data) {
                        if (!!data) {
                            dfd.resolve(data);
                        } else {
                            dfd.reject("Adding RMC:" + rmc + " WishList:" + wishlistId + " data error: null returned");
                        }
                    }
                ).fail(
                    function (qXHR, textStatus) {
                        dfd.reject("Adding RMC:" + rmc + " WishList:" + wishlistId + " data error: " + textStatus);
                    }
                )
            }
            return dfd;
        }


        function deleteFromWishList(wishlistId, rmc) {
            var dfd = new $.Deferred();
            if (isLogged() && wishlistId && rmc) {
                ajaxPost(
                    {
                        "cmd": "deleteFromWishList",
                        "memberId": getMemberData().id,
                        "wishlistId": wishlistId,
                        "rmc": rmc
                    }
                ).done(
                    function (data) {
                        if (!!data) {
                            dfd.resolve(data);
                        } else {
                            dfd.reject("Deleting RMC:" + rmc + " WishList:" + wishlistId + " data error: null returned");
                        }
                    }
                ).fail(
                    function (qXHR, textStatus) {
                        dfd.reject("Deleting RMC:" + rmc + " WishList:" + wishlistId + " data error: " + textStatus);
                    }
                )
            }
            return dfd;
        }

        function loadWishLists() {
            var dfd = new $.Deferred();
            if (isLogged()) {
                ajaxPost(
                    {
                        "cmd": "onlineSimpleWishLists",
                        "memberId": getMemberData().id
                    }
                ).done(
                    function (data) {
                        if (!!data && !!data.wishLists && data.wishLists.length) {
                            dfd.resolve(data.wishLists);

                            // if logged, get the wishlist info to regen the wishlist in header
                            if (isLogged())
                                setTimeout(function () {
                                    loadWishlistsPageInfo();
                                }, 200); // async

                        } else {
                            dfd.resolve(null);
                        }
                    }
                ).fail(
                    function (qXHR, textStatus) {
                        dfd.reject("Loading WishLists data error: " + textStatus);
                    }
                );
            }
            return dfd;
        }

        function loadWishlistsPageInfo(data) {
            var dfd = new $.Deferred(),
                data = data || {},
                request = {
                    cmd: "offlineWishlists",
                    lang: data.lang || RLX.cqlang,
                    mobile: (typeof data.mobile !== "undefined" ? data.mobile : RLX.GLOBALS.features.isMobile),
                    countryCode: data.countryCode || RLX.RolexGeolocationStore.get("countryCode")
                };

            if (!!data.code)
                request.code = data.code

            if (isLogged() && !data.shared) {
                request.memberId = getMemberData().id;
                request.cmd = "onlineWishlists";

                /*
                 if (!!data.code)
                 request.code = data.code
                 */
            }
            else {
                /*
                 if (!!data.code)
                 return dfd.reject(new Error("Loading WishLists data error: access forbidden (not logged in)"))
                 */

                if (data.shared)
                    request.listRmc = data.sharedListRmc.split("."),
                        request.shared = true
                else
                    request.listRmc = data.listRmc
            }

            ajaxPost(request).then(function (data, textStatus, jqXHR) {
                if (!!data) {
                    dfd.resolve(data);

                    //event listened by the wishlist in header
                    RLX.GLOBALS.DOM.jDoc.trigger("mo-wishlist:fetch", data);
                } else
                    dfd.reject(new Error("Loading WishLists data error: no data"))
            }, function (data, textStatus, err) {
                dfd.reject("Loading WishLists data error: " + textStatus);
            });

            return dfd;
        }

        function createWishList(name, order) {
            var dfd = new $.Deferred();
            if (isLogged()) {
                name = name || DEFAULT_WISH_LIST;
                order = order || DEFAULT_ORDER;

                ajaxPost(
                    {
                        "cmd": "createWishList",
                        "memberId": getMemberData().id,
                        "name": name,
                        "order": order
                    }
                ).done(
                    function (data) {
                        if (data) {
                            dfd.resolve(data);
                        } else {
                            dfd.reject("Creating WishList:" + name + " data error: null returned");
                        }
                    }
                ).fail(
                    function (qXHR, textStatus) {
                        dfd.reject("Creating WishList:" + name + " data error: " + textStatus);
                    }
                );
            } else {
                dfd.reject("Not logged in");
            }

            return dfd;
        }

        function deleteRetailerWishlist(id) {
            var dfd = new $.Deferred();

            if (!isLogged()) {
                dfd.reject(new Error("user must be logged in"))

                return dfd//don't go further
            }

            ajaxPost({
                "cmd": "deleteWishList",
                "memberId": getMemberData().id,
                "wishlistId": id
            }).then(function (data) {
                dfd.resolve(data)
            }, function (e) {
                dfd.reject(e)
            })

            return dfd
        }

        function saveRetailerWishList(wishlistId) {
            var dfd = new $.Deferred();
            if (isLogged() && typeof wishlistId === "string") {
                ajaxPost(
                    {
                        "cmd": "saveWishList",
                        "memberId": getMemberData().id,
                        "wishlistId": wishlistId
                    }
                ).done(
                    function (data) {
                        if (data) {
                            dfd.resolve(data);
                        } else {
                            dfd.reject("Saving WishList with id:" + wishlistId + " data error: null returned");
                        }
                    }
                ).fail(
                    function (qXHR, textStatus) {
                        dfd.reject("Saving WishList with id:" + wishlistId + " data error: " + textStatus);
                    }
                );
            } else {
                dfd.reject("Not logged in or wishlist id is empty");
            }

            return dfd;
        }

        function createDefaultWishList() {
            return createWishList(DEFAULT_WISH_LIST, DEFAULT_ORDER).done(loadWishLists);
        }

        //FAVORITE RETAILER PART
        function loadDealers() {
            var dfd = new $.Deferred();
            if (isLogged()) {
                ajaxPost(
                    {
                        "cmd": "onlineSimpleDealers",
                        "memberId": getMemberData().id
                    }
                ).done(
                    function (data) {
                        if (!!data && !!data.dealers && data.dealers.length) {
                            dfd.resolve(data.dealers);
                        } else {
                            dfd.resolve(null);
                        }
                    }
                ).fail(
                    function (qXHR, textStatus) {
                        dfd.reject("Loading Dealer data error: " + textStatus);
                    }
                );
            }
            return dfd;
        }

        function addToDealer(favorRetailer) {
            var dfd = new $.Deferred();
            if (isLogged() && !!favorRetailer) {
                ajaxPost(
                    {
                        "cmd": "addToDealer",
                        "memberId": getMemberData().id,
                        "favorReseller": favorRetailer
                    }
                ).done(
                    function (data) {
                        if (!!data) {
                            dfd.resolve(data);
                        } else {
                            dfd.reject("Adding favorRetailer:" + favorRetailer + " data error: null returned");
                        }
                    }
                ).fail(
                    function (qXHR, textStatus) {
                        dfd.reject("Adding favorRetailer:" + favorRetailer + " data error: " + textStatus);
                    }
                )
            }
            return dfd;
        }

        function ajaxPost(data, reqHeaders) {
            var headers = reqHeaders || {};
            return $.ajax({
                type: "POST",
                url: config.servletPath,
                headers: headers,
                data: data,
                dataType: "json"
            });
        }

        return {
            isLogged: isLogged,
            getMemberData: getMemberData,
            loadWishLists: loadWishLists,
            loadWishlistsPageInfo: loadWishlistsPageInfo,
            createWishList: createWishList,
            saveRetailerWishList: saveRetailerWishList,
            deleteRetailerWishlist: deleteRetailerWishlist,
            createDefaultWishList: createDefaultWishList,
            removeFromDefaultWishList: removeFromDefaultWishList,
            addToDefaultWishList: addToDefaultWishList,
            loadDealers: loadDealers,
            addToDealer: addToDealer
        }

    });
