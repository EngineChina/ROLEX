define("lib/scrollwatcher", ["lib/polyfills"], function(__WEBPACK_EXTERNAL_MODULE__0__) { return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 5);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE__0__;

/***/ }),

/***/ 5:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: external "lib/polyfills"
var polyfills_ = __webpack_require__(0);

// CONCATENATED MODULE: ./src/getBoundingClientRect.js

/*
 * normalized version of Node::getBoundClientRect
 * should return the same values on all browsers
 */

/* harmony default export */ var getBoundingClientRect = (function (node) {
  var bcr = node.getBoundingClientRect();
  var clientT = document.documentElement.clientTop || document.body.clientTop || 0;
  var clientL = document.documentElement.clientLeft || document.body.clientLeft || 0;
  var offsetX = window.pageXOffset || window.scrollX || document.documentElement.scrollLeft || document.body.scrollLeft || 0;
  var offsetY = window.pageYOffset || window.scrollY || document.documentElement.scrollTop || document.body.scrollTop || 0;
  return {
    left: node === document.documentElement || node === document.body ? offsetX : bcr.left + offsetX - clientL,
    top: node === document.documentElement || node === document.body ? offsetY : bcr.top + offsetY - clientT,
    width: bcr.width || bcr.right - bcr.left,
    contentWidth: node.scrollWidth,
    height: bcr.height || bcr.bottom - bcr.top,
    contentHeight: node.scrollHeight
  };
});
// CONCATENATED MODULE: ./src/ScrollWatcher.js
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ScrollWatcher; });


var _window2;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



var watched = new Set(); // nodes that are being tracked at time T

var referrals = new WeakMap(); // nodes whose scroll event are being listened at time T

var watchers = new WeakMap();
var sbcr = new Object(Symbol("bcr"));
var srbcr = new Object(Symbol("referral_bcr"));
var sevent = new Object(Symbol("event"));
var snode = new Object(Symbol("node"));
var smatrix = new Object(Symbol("matrix"));
var sreferral = new Object(Symbol("referral"));
var sthreshold = new Object(Symbol("threshold"));
var obs_timer;
var watching = false;

var getOrigin = function getOrigin() {
  return {
    left: window.pageXOffset || window.scrollX || document.documentElement.scrollLeft || document.body.scrollLeft || 0,
    top: window.pageYOffset || window.scrollY || document.documentElement.scrollTop || document.body.scrollTop || 0
  };
};

var _compute = function compute(node) {
  var dispatchEvent = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

  var _ref = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : getOrigin(),
      originLeft = _ref.left,
      originTop = _ref.top;

  var watcher = watchers.get(node);
  var referral = watcher.referral;
  if (document.documentElement.compareDocumentPosition(node) & Node.DOCUMENT_POSITION_DISCONNECTED) return;
  if (referral && referral.compareDocumentPosition(node) & Node.DOCUMENT_POSITION_DISCONNECTED && node.assignedSlot && referral.compareDocumentPosition(node.assignedSlot) & Node.DOCUMENT_POSITION_DISCONNECTED) return;
  watchers.get(watcher).set(sbcr, getBoundingClientRect(node));
  var threshold = watcher.threshold;
  var _window = window,
      screenwidth = _window.innerWidth,
      screenheight = _window.innerHeight;

  var _ref2 = !referral ? function () {
    var _watcher$bcr = watcher.bcr,
        width = _watcher$bcr.width,
        left = _watcher$bcr.left,
        height = _watcher$bcr.height,
        top = _watcher$bcr.top;
    var offsetY = top - Math.abs(originTop);
    var offsetX = left - Math.abs(originLeft);
    var visibleheight = offsetY > screenheight || offsetY < -height ? 0 : Math.min(height, screenheight, screenheight - offsetY, height + offsetY);
    var visiblewidth = offsetX > screenwidth || offsetX < -width ? 0 : Math.min(width, screenwidth, screenwidth - offsetX, width + offsetX);
    var coverageX = width === 0 ? 0 : visiblewidth / width * 100;
    var coverageY = height === 0 ? 0 : visibleheight / height * 100;
    var coverage = width * height === 0 ? 0 : visiblewidth * visibleheight / (width * height) * 100;
    var visibility = !!coverage && coverage > threshold;
    var overflowX = !visibility ? 0 : Math.max(0, screenwidth - offsetX - width);
    var overflowY = !visibility ? 0 : Math.max(0, screenheight - offsetY - height);
    var progressX = 1 - (offsetX + width) / (width + screenwidth);
    var progressY = 1 - (offsetY + height) / (height + screenheight);
    var offset = offsetY;
    var overflow = overflowY;
    var progress = progressY;
    return {
      height: height,
      top: top,
      left: left,
      width: width,
      offset: offset,
      offsetX: offsetX,
      offsetY: offsetY,
      visibleheight: visibleheight,
      visiblewidth: visiblewidth,
      coverage: coverage,
      coverageX: coverageX,
      coverageY: coverageY,
      visibility: visibility,
      overflow: overflow,
      overflowX: overflowX,
      overflowY: overflowY,
      progress: progress,
      progressX: progressX,
      progressY: progressY
    };
  }() : function () {
    watchers.get(watcher).set(srbcr, getBoundingClientRect(referral));
    var _watcher$rbcr = watcher.rbcr,
        rwidth = _watcher$rbcr.width,
        rleft = _watcher$rbcr.left,
        rheight = _watcher$rbcr.height,
        rtop = _watcher$rbcr.top;
    var _watcher$bcr2 = watcher.bcr,
        width = _watcher$bcr2.width,
        left = _watcher$bcr2.left,
        height = _watcher$bcr2.height,
        top = _watcher$bcr2.top;
    var roffsetX = rleft - Math.abs(originLeft);
    var roffsetY = rtop - Math.abs(originTop);
    var offsetX = left - Math.abs(originLeft);
    var offsetY = top - Math.abs(originTop);
    var rvisibleheight = roffsetY > screenheight || roffsetY < -rheight ? 0 : Math.min(rheight, screenheight, screenheight - roffsetY, rheight + roffsetY);
    var visibleheight = !rvisibleheight ? 0 : Math.max(0, Math.min(rtop + rvisibleheight, top + height) - Math.max(rtop, top));
    var rvisiblewidth = roffsetX > screenwidth || roffsetX < -rwidth ? 0 : Math.min(rwidth, screenwidth, screenwidth - roffsetX, rwidth + roffsetX);
    var visiblewidth = !rvisiblewidth ? 0 : Math.max(0, Math.min(rleft + rvisiblewidth, left + width) - Math.max(rleft, left));
    var coverageX = width === 0 ? 0 : visiblewidth / width * 100;
    var coverageY = height === 0 ? 0 : visibleheight / height * 100;
    var coverage = width * height === 0 ? 0 : visiblewidth * visibleheight / (width * height) * 100;
    var visibility = !!coverage && coverage > threshold;
    var overflowX = !visibility ? 0 : Math.max(0, screenwidth - offsetX - width);
    var overflowY = !visibility ? 0 : Math.max(0, screenheight - offsetY - height);
    var progressX = 1 - (offsetX + width) / (width + screenwidth);
    var progressY = 1 - (offsetY + height) / (height + screenheight);
    var offset = offsetY;
    var overflow = overflowY;
    var progress = progressY;
    return {
      height: height,
      top: top,
      left: left,
      width: width,
      offset: offset,
      offsetX: offsetX,
      offsetY: offsetY,
      visibleheight: visibleheight,
      visiblewidth: visiblewidth,
      coverage: coverage,
      coverageX: coverageX,
      coverageY: coverageY,
      visibility: visibility,
      overflow: overflow,
      overflowX: overflowX,
      overflowY: overflowY,
      progress: progress,
      progressX: progressX,
      progressY: progressY
    };
  }(),
      height = _ref2.height,
      top = _ref2.top,
      width = _ref2.width,
      left = _ref2.left,
      offset = _ref2.offset,
      offsetX = _ref2.offsetX,
      offsetY = _ref2.offsetY,
      visiblewidth = _ref2.visiblewidth,
      visibleheight = _ref2.visibleheight,
      coverage = _ref2.coverage,
      coverageX = _ref2.coverageX,
      coverageY = _ref2.coverageY,
      visibility = _ref2.visibility,
      overflow = _ref2.overflow,
      overflowX = _ref2.overflowX,
      overflowY = _ref2.overflowY,
      progress = _ref2.progress,
      progressX = _ref2.progressX,
      progressY = _ref2.progressY;

  var _watcher$matrix = watcher.matrix,
      prevCoverage = _watcher$matrix.coverage,
      prevCoverageX = _watcher$matrix.coverageX,
      prevCoverageY = _watcher$matrix.coverageY,
      prevOffset = _watcher$matrix.offset,
      prevOffsetX = _watcher$matrix.offsetX,
      prevOffsetY = _watcher$matrix.offsetY,
      prevOverflow = _watcher$matrix.overflow,
      prevOverflowX = _watcher$matrix.overflowX,
      prevOverflowY = _watcher$matrix.overflowY,
      prevVisibility = _watcher$matrix.visibility;
  var deltaX = Math.abs(offsetX) - Math.abs(prevOffsetX || 0);
  var deltaY = Math.abs(offsetY) - Math.abs(prevOffsetY || 0);
  var delta = deltaY; //TODO

  var directionX = deltaX > 0 ? ScrollWatcher.directions.LEFT : ScrollWatcher.directions.RIGHT;
  var directionY = deltaY > 0 ? ScrollWatcher.directions.UP : ScrollWatcher.directions.DOWN;
  var direction = directionX ^ directionY;
  var velocityX = Math.abs(deltaX);
  var velocityY = Math.abs(deltaY);
  var velocity = velocityY; // TODO

  watchers.get(watcher).set(smatrix, {
    coverage: coverage,
    prevCoverage: prevCoverage,
    coverageX: coverageX,
    prevCoverageX: prevCoverageX,
    coverageY: coverageY,
    prevCoverageY: prevCoverageY,
    delta: delta,
    deltaX: deltaX,
    deltaY: deltaY,
    direction: direction,
    width: width,
    height: height,
    offset: offset,
    prevOffset: prevOffset,
    offsetX: offsetX,
    prevOffsetX: prevOffsetX,
    offsetY: offsetY,
    prevOffsetY: prevOffsetY,
    overflow: overflow,
    prevOverflow: prevOverflow,
    overflowX: overflowX,
    prevOverflowX: prevOverflowX,
    overflowY: overflowY,
    prevOverflowY: prevOverflowY,
    progress: progress,
    progressX: progressX,
    progressY: progressY,
    left: left,
    top: top,
    velocity: velocity,
    visibility: visibility
  });

  if (dispatchEvent) {
    if (prevVisibility !== visibility) {
      var event = document.createEvent("event");
      event.initEvent(ScrollWatcher.events.visibilitychange, true, true);
      Object.defineProperty(event, "visibility", {
        enumerable: true,
        value: visibility
      });
      node.dispatchEvent(event);
    }

    if (prevOffsetY !== offsetY || prevOffsetX !== offsetX) {
      var _event = document.createEvent("event");

      _event.initEvent(ScrollWatcher.events.offsetchange, true, true);

      Object.defineProperty(_event, "matrix", {
        enumerable: true,
        get: function get() {
          return watchers.get(watcher).get(smatrix);
        }
      });
      node.dispatchEvent(_event);
    }
  }

  return watchers.get(watcher).get(smatrix);
};

var watchHandler = function watchHandler(_ref3) {
  var target = _ref3.target;
  var origin = getOrigin();
  if (referrals.has(target)) referrals.get(target).forEach(function (node) {
    return _compute(node, true, origin);
  });else if (watched.has(target)) _compute(target, true, origin);else watched.forEach(function (node) {
    return _compute(node, true, origin);
  });
};

var ScrollWatcher =
/*#__PURE__*/
function () {
  _createClass(ScrollWatcher, null, [{
    key: "addScrollSource",
    value: function addScrollSource(source, watched) {
      if (!referrals.has(source)) referrals.set(source, new Set([watched])), source.addEventListener("scroll", ScrollWatcher.watchHandler);else referrals.get(source).add(watched);
    }
  }, {
    key: "removeScrollSource",
    value: function removeScrollSource(source, watched) {
      if (!referrals.has(source)) return;
      referrals.get(source).delete(watched);
      if (!referrals.get(source).size) referrals.delete(source), source.removeEventListener("scroll", ScrollWatcher.watchHandler);
    }
  }, {
    key: "unwatch",
    value: function unwatch() {
      if (!ScrollWatcher.watching || ScrollWatcher.watched.size) return;
      watching = false;
      window.removeEventListener("scroll", function () {
        return ScrollWatcher.watchHandler(false);
      });
      window.removeEventListener("resize", ScrollWatcher.watchHandler);
      window.removeEventListener("orientationchange", ScrollWatcher.watchHandler);
      ScrollWatcher.observer.disconnect();
    }
  }, {
    key: "watch",
    value: function watch() {
      if (ScrollWatcher.watching || !ScrollWatcher.watched.size) return;
      watching = true;
      window.addEventListener("scroll", ScrollWatcher.watchHandler);
      window.addEventListener("resize", ScrollWatcher.watchHandler);
      window.addEventListener("orientationchange", ScrollWatcher.watchHandler);
      ScrollWatcher.observer.observe(document.documentElement, {
        childList: true,
        subtree: true
      });
    }
  }, {
    key: "watched",
    get: function get() {
      return watched;
    }
  }, {
    key: "watchHandler",
    get: function get() {
      return watchHandler;
    }
  }, {
    key: "watching",
    get: function get() {
      return watching;
    }
  }]);

  function ScrollWatcher(node) {
    var _ref4 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
        _ref4$threshold = _ref4.threshold,
        threshold = _ref4$threshold === void 0 ? 0 : _ref4$threshold,
        _ref4$referral = _ref4.referral,
        referral = _ref4$referral === void 0 ? null : _ref4$referral,
        _ref4$event = _ref4.event,
        event = _ref4$event === void 0 ? "scroll" : _ref4$event;

    _classCallCheck(this, ScrollWatcher);

    if (watchers.has(node)) return watchers.get(node);
    watchers.set(this, new WeakMap());
    watchers.set(node, this); // x-ref

    watchers.get(this).set(snode, node);
    watchers.get(this).set(sevent, event);
    watchers.get(this).set(sreferral, referral);
    watchers.get(this).set(sthreshold, threshold);
    Object.defineProperty(node, "scrollWatcher", {
      enumerable: true,
      value: this
    });
    this.compute(false);
  }

  _createClass(ScrollWatcher, [{
    key: "compute",
    value: function compute() {
      var dispatchEvent = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
      return _compute(this.node, dispatchEvent);
    }
  }, {
    key: "unwatch",
    value: function unwatch() {
      watched.delete(this.node);
      ScrollWatcher.unwatch();
      if (this.referral) ScrollWatcher.removeScrollSource(this.referral, this.node);
      if (this.event) this.node.removeEventListener(this.event, watchHandler);
    }
  }, {
    key: "watch",
    value: function watch() {
      watched.add(this.node);
      ScrollWatcher.watch();
      if (this.referral) ScrollWatcher.addScrollSource(this.referral, this.node);
      if (this.event) this.node.addEventListener(this.event, watchHandler);
    }
  }, {
    key: "bcr",
    get: function get() {
      return watchers.get(this).get(sbcr);
    }
  }, {
    key: "rbcr",
    get: function get() {
      return watchers.get(this).get(srbcr);
    }
  }, {
    key: "event",
    get: function get() {
      return watchers.get(this).get(sevent);
    }
  }, {
    key: "node",
    get: function get() {
      return watchers.get(this).get(snode);
    }
  }, {
    key: "referral",
    get: function get() {
      return watchers.get(this).get(sreferral);
    }
  }, {
    key: "threshold",
    get: function get() {
      return watchers.get(this).get(sthreshold);
    }
  }, {
    key: "matrix",
    get: function get() {
      return watchers.get(this).get(smatrix) || {};
    }
  }, {
    key: "coverage",
    get: function get() {
      var _this$matrix;

      return (_this$matrix = this.matrix) === null || _this$matrix === void 0 ? void 0 : _this$matrix.coverage;
    }
  }, {
    key: "coverageX",
    get: function get() {
      var _this$matrix2;

      return (_this$matrix2 = this.matrix) === null || _this$matrix2 === void 0 ? void 0 : _this$matrix2.coverageX;
    }
  }, {
    key: "coverageY",
    get: function get() {
      var _this$matrix3;

      return (_this$matrix3 = this.matrix) === null || _this$matrix3 === void 0 ? void 0 : _this$matrix3.coverage;
    }
  }, {
    key: "offset",
    get: function get() {
      var _this$matrix4;

      return (_this$matrix4 = this.matrix) === null || _this$matrix4 === void 0 ? void 0 : _this$matrix4.offset;
    }
  }, {
    key: "offsetX",
    get: function get() {
      var _this$matrix5;

      return (_this$matrix5 = this.matrix) === null || _this$matrix5 === void 0 ? void 0 : _this$matrix5.offseYtX;
    }
  }, {
    key: "offsetY",
    get: function get() {
      var _this$matrix6;

      return (_this$matrix6 = this.matrix) === null || _this$matrix6 === void 0 ? void 0 : _this$matrix6.offsetY;
    }
  }, {
    key: "overflow",
    get: function get() {
      var _this$matrix7;

      return (_this$matrix7 = this.matrix) === null || _this$matrix7 === void 0 ? void 0 : _this$matrix7.overflow;
    }
  }, {
    key: "overflowX",
    get: function get() {
      var _this$matrix8;

      return (_this$matrix8 = this.matrix) === null || _this$matrix8 === void 0 ? void 0 : _this$matrix8.overflowX;
    }
  }, {
    key: "overflowY",
    get: function get() {
      var _this$matrix9;

      return (_this$matrix9 = this.matrix) === null || _this$matrix9 === void 0 ? void 0 : _this$matrix9.overflowY;
    }
  }, {
    key: "progress",
    get: function get() {
      var _this$matrix10;

      return (_this$matrix10 = this.matrix) === null || _this$matrix10 === void 0 ? void 0 : _this$matrix10.progress;
    }
  }, {
    key: "progressX",
    get: function get() {
      var _this$matrix11;

      return (_this$matrix11 = this.matrix) === null || _this$matrix11 === void 0 ? void 0 : _this$matrix11.progressX;
    }
  }, {
    key: "progressY",
    get: function get() {
      var _this$matrix12;

      return (_this$matrix12 = this.matrix) === null || _this$matrix12 === void 0 ? void 0 : _this$matrix12.progressY;
    }
  }, {
    key: "left",
    get: function get() {
      var _this$matrix13;

      return (_this$matrix13 = this.matrix) === null || _this$matrix13 === void 0 ? void 0 : _this$matrix13.left;
    }
  }, {
    key: "top",
    get: function get() {
      var _this$matrix14;

      return (_this$matrix14 = this.matrix) === null || _this$matrix14 === void 0 ? void 0 : _this$matrix14.top;
    }
  }, {
    key: "width",
    get: function get() {
      var _this$matrix15;

      return (_this$matrix15 = this.matrix) === null || _this$matrix15 === void 0 ? void 0 : _this$matrix15.width;
    }
  }, {
    key: "height",
    get: function get() {
      var _this$matrix16;

      return (_this$matrix16 = this.matrix) === null || _this$matrix16 === void 0 ? void 0 : _this$matrix16.width;
    }
  }, {
    key: "visibility",
    get: function get() {
      var _this$matrix17;

      return (_this$matrix17 = this.matrix) === null || _this$matrix17 === void 0 ? void 0 : _this$matrix17.visibility;
    }
  }]);

  return ScrollWatcher;
}();

_defineProperty(ScrollWatcher, "directions", {
  UP: 1,
  DOWN: 2,
  LEFT: 4,
  RIGHT: 8
});

_defineProperty(ScrollWatcher, "events", {
  visibilitychange: "visibilitychange",
  offsetchange: "offsetchange"
});

_defineProperty(ScrollWatcher, "observer", ((_window2 = window) === null || _window2 === void 0 ? void 0 : _window2.MutationObserver) ? new MutationObserver(function (mutations) {
  cancelAnimationFrame(obs_timer);
  obs_timer = requestAnimationFrame(ScrollWatcher.watchHandler);
}) : {
  observe: function observe() {},
  disconnect: function disconnect() {}
});



/***/ })

/******/ })});;
//# sourceMappingURL=scrollwatcher.js.map