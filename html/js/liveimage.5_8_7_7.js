define("lib/liveimage", ["lib/polyfills"], function(__WEBPACK_EXTERNAL_MODULE__0__) { return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE__0__;

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/*! npm.im/intrinsic-scale */


Object.defineProperty(exports, '__esModule', {
  value: true
});

function fit(contains) {
  return function (parentWidth, parentHeight, childWidth, childHeight) {
    var doRatio = childWidth / childHeight;
    var cRatio = parentWidth / parentHeight;
    var width;
    var height;

    if (contains ? doRatio > cRatio : doRatio < cRatio) {
      width = parentWidth;
      height = width / doRatio;
    } else {
      height = parentHeight;
      width = height * doRatio;
    }

    return {
      width: width,
      height: height,
      x: (parentWidth - width) / 2,
      y: (parentHeight - height) / 2
    };
  };
}

var contain = fit(true);
var cover = fit(false);
exports.contain = contain;
exports.cover = cover;

/***/ }),
/* 2 */,
/* 3 */,
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./src/bound.js


function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/* harmony default export */ var bound = (function (_ref) {
  var kind = _ref.kind,
      key = _ref.key,
      placement = _ref.placement,
      descriptor = _ref.descriptor;
  if (kind !== "method") return {
    kind: kind,
    key: key,
    placement: placement,
    descriptor: descriptor
  };
  return {
    kind: kind,
    key: key,
    placement: placement,
    descriptor: descriptor,
    extras: [{
      kind: "field",
      key: key,
      placement: "own",
      descriptor: _objectSpread({}, descriptor, {
        value: undefined
      }),
      initializer: function initializer() {
        return descriptor.value.bind(this);
      }
    }]
  };
});
// CONCATENATED MODULE: ./src/createEvent.js


/* harmony default export */ var createEvent = (function () {
  var type = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "error";
  var props = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var event = document.createEvent("Event");
  event.initEvent(type, true, true);
  void Object.keys(props).forEach(function (prop) {
    return props.hasOwnProperty(prop) && Object.defineProperty(event, prop, {
      enumerable: true,
      value: props[prop]
    });
  });
  return event;
});
// EXTERNAL MODULE: ./node_modules/intrinsic-scale/dist/intrinsic-scale.common-js.js
var intrinsic_scale_common_js = __webpack_require__(1);

// EXTERNAL MODULE: external "lib/polyfills"
var polyfills_ = __webpack_require__(0);

// CONCATENATED MODULE: ./src/LiveImage.js
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return LiveImage_LiveImage; });


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _decorate(decorators, factory, superClass, mixins) { var api = _getDecoratorsApi(); if (mixins) { for (var i = 0; i < mixins.length; i++) { api = mixins[i](api); } } var r = factory(function initialize(O) { api.initializeInstanceElements(O, decorated.elements); }, superClass); var decorated = api.decorateClass(_coalesceClassElements(r.d.map(_createElementDescriptor)), decorators); api.initializeClassElements(r.F, decorated.elements); return api.runClassFinishers(r.F, decorated.finishers); }

function _getDecoratorsApi() { _getDecoratorsApi = function _getDecoratorsApi() { return api; }; var api = { elementsDefinitionOrder: [["method"], ["field"]], initializeInstanceElements: function initializeInstanceElements(O, elements) { ["method", "field"].forEach(function (kind) { elements.forEach(function (element) { if (element.kind === kind && element.placement === "own") { this.defineClassElement(O, element); } }, this); }, this); }, initializeClassElements: function initializeClassElements(F, elements) { var proto = F.prototype; ["method", "field"].forEach(function (kind) { elements.forEach(function (element) { var placement = element.placement; if (element.kind === kind && (placement === "static" || placement === "prototype")) { var receiver = placement === "static" ? F : proto; this.defineClassElement(receiver, element); } }, this); }, this); }, defineClassElement: function defineClassElement(receiver, element) { var descriptor = element.descriptor; if (element.kind === "field") { var initializer = element.initializer; descriptor = { enumerable: descriptor.enumerable, writable: descriptor.writable, configurable: descriptor.configurable, value: initializer === void 0 ? void 0 : initializer.call(receiver) }; } Object.defineProperty(receiver, element.key, descriptor); }, decorateClass: function decorateClass(elements, decorators) { var newElements = []; var finishers = []; var placements = { static: [], prototype: [], own: [] }; elements.forEach(function (element) { this.addElementPlacement(element, placements); }, this); elements.forEach(function (element) { if (!_hasDecorators(element)) return newElements.push(element); var elementFinishersExtras = this.decorateElement(element, placements); newElements.push(elementFinishersExtras.element); newElements.push.apply(newElements, elementFinishersExtras.extras); finishers.push.apply(finishers, elementFinishersExtras.finishers); }, this); if (!decorators) { return { elements: newElements, finishers: finishers }; } var result = this.decorateConstructor(newElements, decorators); finishers.push.apply(finishers, result.finishers); result.finishers = finishers; return result; }, addElementPlacement: function addElementPlacement(element, placements, silent) { var keys = placements[element.placement]; if (!silent && keys.indexOf(element.key) !== -1) { throw new TypeError("Duplicated element (" + element.key + ")"); } keys.push(element.key); }, decorateElement: function decorateElement(element, placements) { var extras = []; var finishers = []; for (var decorators = element.decorators, i = decorators.length - 1; i >= 0; i--) { var keys = placements[element.placement]; keys.splice(keys.indexOf(element.key), 1); var elementObject = this.fromElementDescriptor(element); var elementFinisherExtras = this.toElementFinisherExtras((0, decorators[i])(elementObject) || elementObject); element = elementFinisherExtras.element; this.addElementPlacement(element, placements); if (elementFinisherExtras.finisher) { finishers.push(elementFinisherExtras.finisher); } var newExtras = elementFinisherExtras.extras; if (newExtras) { for (var j = 0; j < newExtras.length; j++) { this.addElementPlacement(newExtras[j], placements); } extras.push.apply(extras, newExtras); } } return { element: element, finishers: finishers, extras: extras }; }, decorateConstructor: function decorateConstructor(elements, decorators) { var finishers = []; for (var i = decorators.length - 1; i >= 0; i--) { var obj = this.fromClassDescriptor(elements); var elementsAndFinisher = this.toClassDescriptor((0, decorators[i])(obj) || obj); if (elementsAndFinisher.finisher !== undefined) { finishers.push(elementsAndFinisher.finisher); } if (elementsAndFinisher.elements !== undefined) { elements = elementsAndFinisher.elements; for (var j = 0; j < elements.length - 1; j++) { for (var k = j + 1; k < elements.length; k++) { if (elements[j].key === elements[k].key && elements[j].placement === elements[k].placement) { throw new TypeError("Duplicated element (" + elements[j].key + ")"); } } } } } return { elements: elements, finishers: finishers }; }, fromElementDescriptor: function fromElementDescriptor(element) { var obj = { kind: element.kind, key: element.key, placement: element.placement, descriptor: element.descriptor }; var desc = { value: "Descriptor", configurable: true }; Object.defineProperty(obj, Symbol.toStringTag, desc); if (element.kind === "field") obj.initializer = element.initializer; return obj; }, toElementDescriptors: function toElementDescriptors(elementObjects) { if (elementObjects === undefined) return; return _toArray(elementObjects).map(function (elementObject) { var element = this.toElementDescriptor(elementObject); this.disallowProperty(elementObject, "finisher", "An element descriptor"); this.disallowProperty(elementObject, "extras", "An element descriptor"); return element; }, this); }, toElementDescriptor: function toElementDescriptor(elementObject) { var kind = String(elementObject.kind); if (kind !== "method" && kind !== "field") { throw new TypeError('An element descriptor\'s .kind property must be either "method" or' + ' "field", but a decorator created an element descriptor with' + ' .kind "' + kind + '"'); } var key = _toPropertyKey(elementObject.key); var placement = String(elementObject.placement); if (placement !== "static" && placement !== "prototype" && placement !== "own") { throw new TypeError('An element descriptor\'s .placement property must be one of "static",' + ' "prototype" or "own", but a decorator created an element descriptor' + ' with .placement "' + placement + '"'); } var descriptor = elementObject.descriptor; this.disallowProperty(elementObject, "elements", "An element descriptor"); var element = { kind: kind, key: key, placement: placement, descriptor: Object.assign({}, descriptor) }; if (kind !== "field") { this.disallowProperty(elementObject, "initializer", "A method descriptor"); } else { this.disallowProperty(descriptor, "get", "The property descriptor of a field descriptor"); this.disallowProperty(descriptor, "set", "The property descriptor of a field descriptor"); this.disallowProperty(descriptor, "value", "The property descriptor of a field descriptor"); element.initializer = elementObject.initializer; } return element; }, toElementFinisherExtras: function toElementFinisherExtras(elementObject) { var element = this.toElementDescriptor(elementObject); var finisher = _optionalCallableProperty(elementObject, "finisher"); var extras = this.toElementDescriptors(elementObject.extras); return { element: element, finisher: finisher, extras: extras }; }, fromClassDescriptor: function fromClassDescriptor(elements) { var obj = { kind: "class", elements: elements.map(this.fromElementDescriptor, this) }; var desc = { value: "Descriptor", configurable: true }; Object.defineProperty(obj, Symbol.toStringTag, desc); return obj; }, toClassDescriptor: function toClassDescriptor(obj) { var kind = String(obj.kind); if (kind !== "class") { throw new TypeError('A class descriptor\'s .kind property must be "class", but a decorator' + ' created a class descriptor with .kind "' + kind + '"'); } this.disallowProperty(obj, "key", "A class descriptor"); this.disallowProperty(obj, "placement", "A class descriptor"); this.disallowProperty(obj, "descriptor", "A class descriptor"); this.disallowProperty(obj, "initializer", "A class descriptor"); this.disallowProperty(obj, "extras", "A class descriptor"); var finisher = _optionalCallableProperty(obj, "finisher"); var elements = this.toElementDescriptors(obj.elements); return { elements: elements, finisher: finisher }; }, runClassFinishers: function runClassFinishers(constructor, finishers) { for (var i = 0; i < finishers.length; i++) { var newConstructor = (0, finishers[i])(constructor); if (newConstructor !== undefined) { if (typeof newConstructor !== "function") { throw new TypeError("Finishers must return a constructor."); } constructor = newConstructor; } } return constructor; }, disallowProperty: function disallowProperty(obj, name, objectType) { if (obj[name] !== undefined) { throw new TypeError(objectType + " can't have a ." + name + " property."); } } }; return api; }

function _createElementDescriptor(def) { var key = _toPropertyKey(def.key); var descriptor; if (def.kind === "method") { descriptor = { value: def.value, writable: true, configurable: true, enumerable: false }; } else if (def.kind === "get") { descriptor = { get: def.value, configurable: true, enumerable: false }; } else if (def.kind === "set") { descriptor = { set: def.value, configurable: true, enumerable: false }; } else if (def.kind === "field") { descriptor = { configurable: true, writable: true, enumerable: true }; } var element = { kind: def.kind === "field" ? "field" : "method", key: key, placement: def.static ? "static" : def.kind === "field" ? "own" : "prototype", descriptor: descriptor }; if (def.decorators) element.decorators = def.decorators; if (def.kind === "field") element.initializer = def.value; return element; }

function _coalesceGetterSetter(element, other) { if (element.descriptor.get !== undefined) { other.descriptor.get = element.descriptor.get; } else { other.descriptor.set = element.descriptor.set; } }

function _coalesceClassElements(elements) { var newElements = []; var isSameElement = function isSameElement(other) { return other.kind === "method" && other.key === element.key && other.placement === element.placement; }; for (var i = 0; i < elements.length; i++) { var element = elements[i]; var other; if (element.kind === "method" && (other = newElements.find(isSameElement))) { if (_isDataDescriptor(element.descriptor) || _isDataDescriptor(other.descriptor)) { if (_hasDecorators(element) || _hasDecorators(other)) { throw new ReferenceError("Duplicated methods (" + element.key + ") can't be decorated."); } other.descriptor = element.descriptor; } else { if (_hasDecorators(element)) { if (_hasDecorators(other)) { throw new ReferenceError("Decorators can't be placed on different accessors with for " + "the same property (" + element.key + ")."); } other.decorators = element.decorators; } _coalesceGetterSetter(element, other); } } else { newElements.push(element); } } return newElements; }

function _hasDecorators(element) { return element.decorators && element.decorators.length; }

function _isDataDescriptor(desc) { return desc !== undefined && !(desc.value === undefined && desc.writable === undefined); }

function _optionalCallableProperty(obj, name) { var value = obj[name]; if (value !== undefined && typeof value !== "function") { throw new TypeError("Expected '" + name + "' to be a function"); } return value; }

function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }

function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }

function _toArray(arr) { return _arrayWithHoles(arr) || _iterableToArray(arr) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }





var liveimages = new WeakMap();
var liveimagesSets = new WeakMap();
var sli = new Object(Symbol("live_image"));
var sactive = new Object(Symbol("active"));
var scanvas = new Object(Symbol("canvas"));
var sparent = new Object(Symbol("parent"));
var sposter = new Object(Symbol("poster"));
var sstart = new Object(Symbol("start"));
var send = new Object(Symbol("end"));
var svideo = new Object(Symbol("video"));
var sraf = new Object(Symbol("raf"));
var snode = new Object(Symbol("node"));
var ssrc = new Object(Symbol("src"));
var sctx = new Object(Symbol("ctx"));
var scstyle = new Object(Symbol("computedStyle"));
var ssources = new Object(Symbol("sources"));
var swidth = new Object(Symbol("width"));
var sheight = new Object(Symbol("height"));
var smql = new Object(Symbol("mql"));
var ssaved = new Object(Symbol("saved"));
var spointer = new Object(Symbol("pointer"));
var squality = new Object(Symbol("quality"));
var shrt = new Object(Symbol("hrt"));
var smode = new Object(Symbol("mode"));
var debug = location.href.indexOf("debug=1") !== -1;
var dummy = document.createElement("div");

var canPlayWithoutAction = _asyncToGenerator(
/*#__PURE__*/
regeneratorRuntime.mark(function _callee() {
  var node;
  return regeneratorRuntime.wrap(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          node = document.createElement("video");
          node.muted = true;
          node.setAttribute("playsinline", true);
          node.setAttribute("webkit-playsinline", true); // 1x1 empty video

          node.src = "data:video/mp4;base64, AAAAHGZ0eXBNNFYgAAACAGlzb21pc28yYXZjMQAAAAhmcmVlAAAGF21kYXTeBAAAbGliZmFhYyAxLjI4AABCAJMgBDIARwAAArEGBf//rdxF6b3m2Ui3lizYINkj7u94MjY0IC0gY29yZSAxNDIgcjIgOTU2YzhkOCAtIEguMjY0L01QRUctNCBBVkMgY29kZWMgLSBDb3B5bGVmdCAyMDAzLTIwMTQgLSBodHRwOi8vd3d3LnZpZGVvbGFuLm9yZy94MjY0Lmh0bWwgLSBvcHRpb25zOiBjYWJhYz0wIHJlZj0zIGRlYmxvY2s9MTowOjAgYW5hbHlzZT0weDE6MHgxMTEgbWU9aGV4IHN1Ym1lPTcgcHN5PTEgcHN5X3JkPTEuMDA6MC4wMCBtaXhlZF9yZWY9MSBtZV9yYW5nZT0xNiBjaHJvbWFfbWU9MSB0cmVsbGlzPTEgOHg4ZGN0PTAgY3FtPTAgZGVhZHpvbmU9MjEsMTEgZmFzdF9wc2tpcD0xIGNocm9tYV9xcF9vZmZzZXQ9LTIgdGhyZWFkcz02IGxvb2thaGVhZF90aHJlYWRzPTEgc2xpY2VkX3RocmVhZHM9MCBucj0wIGRlY2ltYXRlPTEgaW50ZXJsYWNlZD0wIGJsdXJheV9jb21wYXQ9MCBjb25zdHJhaW5lZF9pbnRyYT0wIGJmcmFtZXM9MCB3ZWlnaHRwPTAga2V5aW50PTI1MCBrZXlpbnRfbWluPTI1IHNjZW5lY3V0PTQwIGludHJhX3JlZnJlc2g9MCByY19sb29rYWhlYWQ9NDAgcmM9Y3JmIG1idHJlZT0xIGNyZj0yMy4wIHFjb21wPTAuNjAgcXBtaW49MCBxcG1heD02OSBxcHN0ZXA9NCB2YnZfbWF4cmF0ZT03NjggdmJ2X2J1ZnNpemU9MzAwMCBjcmZfbWF4PTAuMCBuYWxfaHJkPW5vbmUgZmlsbGVyPTAgaXBfcmF0aW89MS40MCBhcT0xOjEuMDAAgAAAAFZliIQL8mKAAKvMnJycnJycnJycnXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXiEASZACGQAjgCEASZACGQAjgAAAAAdBmjgX4GSAIQBJkAIZACOAAAAAB0GaVAX4GSAhAEmQAhkAI4AhAEmQAhkAI4AAAAAGQZpgL8DJIQBJkAIZACOAIQBJkAIZACOAAAAABkGagC/AySEASZACGQAjgAAAAAZBmqAvwMkhAEmQAhkAI4AhAEmQAhkAI4AAAAAGQZrAL8DJIQBJkAIZACOAAAAABkGa4C/AySEASZACGQAjgCEASZACGQAjgAAAAAZBmwAvwMkhAEmQAhkAI4AAAAAGQZsgL8DJIQBJkAIZACOAIQBJkAIZACOAAAAABkGbQC/AySEASZACGQAjgCEASZACGQAjgAAAAAZBm2AvwMkhAEmQAhkAI4AAAAAGQZuAL8DJIQBJkAIZACOAIQBJkAIZACOAAAAABkGboC/AySEASZACGQAjgAAAAAZBm8AvwMkhAEmQAhkAI4AhAEmQAhkAI4AAAAAGQZvgL8DJIQBJkAIZACOAAAAABkGaAC/AySEASZACGQAjgCEASZACGQAjgAAAAAZBmiAvwMkhAEmQAhkAI4AhAEmQAhkAI4AAAAAGQZpAL8DJIQBJkAIZACOAAAAABkGaYC/AySEASZACGQAjgCEASZACGQAjgAAAAAZBmoAvwMkhAEmQAhkAI4AAAAAGQZqgL8DJIQBJkAIZACOAIQBJkAIZACOAAAAABkGawC/AySEASZACGQAjgAAAAAZBmuAvwMkhAEmQAhkAI4AhAEmQAhkAI4AAAAAGQZsAL8DJIQBJkAIZACOAAAAABkGbIC/AySEASZACGQAjgCEASZACGQAjgAAAAAZBm0AvwMkhAEmQAhkAI4AhAEmQAhkAI4AAAAAGQZtgL8DJIQBJkAIZACOAAAAABkGbgCvAySEASZACGQAjgCEASZACGQAjgAAAAAZBm6AnwMkhAEmQAhkAI4AhAEmQAhkAI4AhAEmQAhkAI4AhAEmQAhkAI4AAAAhubW9vdgAAAGxtdmhkAAAAAAAAAAAAAAAAAAAD6AAABDcAAQAAAQAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwAAAzB0cmFrAAAAXHRraGQAAAADAAAAAAAAAAAAAAABAAAAAAAAA+kAAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAABAAAAAALAAAACQAAAAAAAkZWR0cwAAABxlbHN0AAAAAAAAAAEAAAPpAAAAAAABAAAAAAKobWRpYQAAACBtZGhkAAAAAAAAAAAAAAAAAAB1MAAAdU5VxAAAAAAALWhkbHIAAAAAAAAAAHZpZGUAAAAAAAAAAAAAAABWaWRlb0hhbmRsZXIAAAACU21pbmYAAAAUdm1oZAAAAAEAAAAAAAAAAAAAACRkaW5mAAAAHGRyZWYAAAAAAAAAAQAAAAx1cmwgAAAAAQAAAhNzdGJsAAAAr3N0c2QAAAAAAAAAAQAAAJ9hdmMxAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAALAAkABIAAAASAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGP//AAAALWF2Y0MBQsAN/+EAFWdCwA3ZAsTsBEAAAPpAADqYA8UKkgEABWjLg8sgAAAAHHV1aWRraEDyXyRPxbo5pRvPAyPzAAAAAAAAABhzdHRzAAAAAAAAAAEAAAAeAAAD6QAAABRzdHNzAAAAAAAAAAEAAAABAAAAHHN0c2MAAAAAAAAAAQAAAAEAAAABAAAAAQAAAIxzdHN6AAAAAAAAAAAAAAAeAAADDwAAAAsAAAALAAAACgAAAAoAAAAKAAAACgAAAAoAAAAKAAAACgAAAAoAAAAKAAAACgAAAAoAAAAKAAAACgAAAAoAAAAKAAAACgAAAAoAAAAKAAAACgAAAAoAAAAKAAAACgAAAAoAAAAKAAAACgAAAAoAAAAKAAAAiHN0Y28AAAAAAAAAHgAAAEYAAANnAAADewAAA5gAAAO0AAADxwAAA+MAAAP2AAAEEgAABCUAAARBAAAEXQAABHAAAASMAAAEnwAABLsAAATOAAAE6gAABQYAAAUZAAAFNQAABUgAAAVkAAAFdwAABZMAAAWmAAAFwgAABd4AAAXxAAAGDQAABGh0cmFrAAAAXHRraGQAAAADAAAAAAAAAAAAAAACAAAAAAAABDcAAAAAAAAAAAAAAAEBAAAAAAEAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAkZWR0cwAAABxlbHN0AAAAAAAAAAEAAAQkAAADcAABAAAAAAPgbWRpYQAAACBtZGhkAAAAAAAAAAAAAAAAAAC7gAAAykBVxAAAAAAALWhkbHIAAAAAAAAAAHNvdW4AAAAAAAAAAAAAAABTb3VuZEhhbmRsZXIAAAADi21pbmYAAAAQc21oZAAAAAAAAAAAAAAAJGRpbmYAAAAcZHJlZgAAAAAAAAABAAAADHVybCAAAAABAAADT3N0YmwAAABnc3RzZAAAAAAAAAABAAAAV21wNGEAAAAAAAAAAQAAAAAAAAAAAAIAEAAAAAC7gAAAAAAAM2VzZHMAAAAAA4CAgCIAAgAEgICAFEAVBbjYAAu4AAAADcoFgICAAhGQBoCAgAECAAAAIHN0dHMAAAAAAAAAAgAAADIAAAQAAAAAAQAAAkAAAAFUc3RzYwAAAAAAAAAbAAAAAQAAAAEAAAABAAAAAgAAAAIAAAABAAAAAwAAAAEAAAABAAAABAAAAAIAAAABAAAABgAAAAEAAAABAAAABwAAAAIAAAABAAAACAAAAAEAAAABAAAACQAAAAIAAAABAAAACgAAAAEAAAABAAAACwAAAAIAAAABAAAADQAAAAEAAAABAAAADgAAAAIAAAABAAAADwAAAAEAAAABAAAAEAAAAAIAAAABAAAAEQAAAAEAAAABAAAAEgAAAAIAAAABAAAAFAAAAAEAAAABAAAAFQAAAAIAAAABAAAAFgAAAAEAAAABAAAAFwAAAAIAAAABAAAAGAAAAAEAAAABAAAAGQAAAAIAAAABAAAAGgAAAAEAAAABAAAAGwAAAAIAAAABAAAAHQAAAAEAAAABAAAAHgAAAAIAAAABAAAAHwAAAAQAAAABAAAA4HN0c3oAAAAAAAAAAAAAADMAAAAaAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAACMc3RjbwAAAAAAAAAfAAAALAAAA1UAAANyAAADhgAAA6IAAAO+AAAD0QAAA+0AAAQAAAAEHAAABC8AAARLAAAEZwAABHoAAASWAAAEqQAABMUAAATYAAAE9AAABRAAAAUjAAAFPwAABVIAAAVuAAAFgQAABZ0AAAWwAAAFzAAABegAAAX7AAAGFwAAAGJ1ZHRhAAAAWm1ldGEAAAAAAAAAIWhkbHIAAAAAAAAAAG1kaXJhcHBsAAAAAAAAAAAAAAAALWlsc3QAAAAlqXRvbwAAAB1kYXRhAAAAAQAAAABMYXZmNTUuMzMuMTAw";
          _context.prev = 5;
          _context.next = 8;
          return node.play();

        case 8:
          _context.next = 14;
          break;

        case 10:
          _context.prev = 10;
          _context.t0 = _context["catch"](5);

          if (!(_context.t0.name === "NotAllowedError")) {
            _context.next = 14;
            break;
          }

          return _context.abrupt("return", false);

        case 14:
          return _context.abrupt("return", true);

        case 15:
        case "end":
          return _context.stop();
      }
    }
  }, _callee, null, [[5, 10]]);
}))(); // debug lines! keep commented
// EventTarget.prototype.addEventListener = (function(native) {
//     return function(...args){
//         console.log("add", this.nodeName, args)
//         this::native(...args)
//     }
// })(EventTarget.prototype.addEventListener)
//
// EventTarget.prototype.removeEventListener = (function(native) {
//     return function(...args){
//         console.log("remove", this.nodeName, args)
//         this::native(...args)
//     }
// })(EventTarget.prototype.removeEventListener)


var offcanvas = document.createElement("canvas");
var offcanvasctx = offcanvas.getContext("2d"); //document.body.insertBefore(offcanvas, document.body.childNodes[0])

var LiveImage_LiveImage = _decorate(null, function (_initialize) {
  var LiveImage = function LiveImage() {
    var _ref2 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
        parent = _ref2.parent,
        _ref2$quality = _ref2.quality,
        quality = _ref2$quality === void 0 ? 1 : _ref2$quality,
        _ref2$mode = _ref2.mode,
        mode = _ref2$mode === void 0 ? "contain" : _ref2$mode,
        _ref2$canvas = _ref2.canvas;

    _ref2$canvas = _ref2$canvas === void 0 ? {} : _ref2$canvas;
    var _ref2$canvas$target = _ref2$canvas.target,
        canvas = _ref2$canvas$target === void 0 ? null : _ref2$canvas$target,
        _ref2$canvas$alpha = _ref2$canvas.alpha,
        alpha = _ref2$canvas$alpha === void 0 ? false : _ref2$canvas$alpha,
        _ref2$canvas$classNam = _ref2$canvas.className,
        canvas_className = _ref2$canvas$classNam === void 0 ? null : _ref2$canvas$classNam,
        _ref2$video = _ref2.video;
    _ref2$video = _ref2$video === void 0 ? {} : _ref2$video;
    var _ref2$video$source = _ref2$video.source,
        video_source = _ref2$video$source === void 0 ? [] : _ref2$video$source,
        _ref2$video$muted = _ref2$video.muted,
        muted = _ref2$video$muted === void 0 ? true : _ref2$video$muted,
        _ref2$video$loop = _ref2$video.loop,
        loop = _ref2$video$loop === void 0 ? false : _ref2$video$loop,
        _ref2$video$playbackR = _ref2$video.playbackRate,
        playbackRate = _ref2$video$playbackR === void 0 ? 1 : _ref2$video$playbackR,
        _ref2$poster = _ref2.poster;
    _ref2$poster = _ref2$poster === void 0 ? {} : _ref2$poster;
    var _ref2$poster$start = _ref2$poster.start;
    _ref2$poster$start = _ref2$poster$start === void 0 ? {} : _ref2$poster$start;
    var _ref2$poster$start$so = _ref2$poster$start.source,
        poster_start_source = _ref2$poster$start$so === void 0 ? [] : _ref2$poster$start$so,
        _ref2$poster$end = _ref2$poster.end;
    _ref2$poster$end = _ref2$poster$end === void 0 ? {} : _ref2$poster$end;
    var _ref2$poster$end$sour = _ref2$poster$end.source,
        poster_end_source = _ref2$poster$end$sour === void 0 ? [] : _ref2$poster$end$sour;

    _classCallCheck(this, LiveImage);

    _initialize(this);

    if (!liveimagesSets.has(parent)) liveimagesSets.set(parent, new WeakMap([[sli, new Set()], [sactive, null]]));
    canvas = canvas || document.createElement("canvas");
    if (canvas_className) canvas.classList.add(canvas_className);
    Object.defineProperty(canvas, "liveImage", {
      enumerable: true,
      value: this
    });
    var ctx = canvas.getContext("2d", {
      alpha: alpha
    });
    var poster_start = document.createElement("img");
    var poster_end = document.createElement("img");
    liveimages.set(this, new WeakMap([[sparent, parent], [squality, quality], [spointer, null], [smode, mode], [shrt, 0], [scanvas, new WeakMap([[snode, canvas], [sctx, ctx], [scstyle, getComputedStyle(canvas)]])], [svideo, new WeakMap([//[snode, video], [ssources, video_source], [smql, new Set]
    [snode, null], [ssources, video_source], [smql, new Set()]])], [sposter, new WeakMap([[sstart, new WeakMap([[snode, poster_start], [ssources, poster_start_source], [smql, new Set()]])], [send, new WeakMap([[snode, poster_end], [ssources, poster_end_source], [smql, new Set()]])]])]]));
  }
  /* event Emitting */
  ;

  return {
    F: LiveImage,
    d: [{
      kind: "field",
      static: true,
      key: "cssTextForIntrinsicDimensions",
      value: function value() {
        return "box-sizing:content-box!important;width:auto!important;height:auto!important;display:block!important;margin:0!important;padding:0!important;transform:scale(1)!important;";
      }
    }, {
      kind: "field",
      static: true,
      key: "events",
      value: function value() {
        return {
          frame: "li:frame",
          videosrcchange: "video:src:change",
          posterstartsrcchange: "poster:start:src:change",
          posterendsrcchange: "poster:end:src:change"
        };
      }
    }, {
      kind: "field",
      static: true,
      key: "scale",
      value: function value() {
        return {
          contain: function contain(source, sw, sh, w, h) {
            var _contain2 = Object(intrinsic_scale_common_js["contain"])(sw, sh, w, h),
                x = _contain2.x,
                y = _contain2.y,
                width = _contain2.width,
                height = _contain2.height;

            return [source, x, y, width, height, 0, 0, w, h];
          },
          cover: function cover(source, sw, sh, w, h) {
            offcanvasctx.restore();
            offcanvasctx.save();

            var _cover2 = Object(intrinsic_scale_common_js["cover"])(w, h, sw, sh),
                width = _cover2.width,
                height = _cover2.height;

            source.width = width;
            source.height = height;
            offcanvas.width = Math.max(sw, width);
            offcanvas.height = Math.max(sh, height);
            var scaleW = width / sw;
            var scaleH = height / sh; //console.log(scaleW, scaleH)

            offcanvasctx.scale(scaleW, scaleH);
            offcanvasctx.drawImage(source, 0, 0); //console.log("source", sw, sh, "int", width, height, "target", w, h)

            var tmpx = Math.max(w, width) - Math.min(w, width);
            var tmpy = Math.max(h, height) - Math.min(h, height);
            var dx = tmpx ? tmpx / 2 : tmpx;
            var dh = tmpy ? tmpy / 2 : tmpy; //console.log("dx, dh", dx, dh)

            return [offcanvas, -dx, -dh];
          }
        };
      }
    }, {
      kind: "method",
      key: "addEventListener",
      value: function addEventListener() {
        var _this$canvas;

        (_this$canvas = this.canvas).addEventListener.apply(_this$canvas, arguments);
      }
    }, {
      kind: "method",
      key: "dispatchEvent",
      value: function dispatchEvent() {
        var _this$canvas2;

        (_this$canvas2 = this.canvas).dispatchEvent.apply(_this$canvas2, arguments);
      }
    }, {
      kind: "method",
      key: "removeEventListener",
      value: function removeEventListener() {
        var _this$canvas3;

        (_this$canvas3 = this.canvas).removeEventListener.apply(_this$canvas3, arguments);
      }
      /* sets/units */

    }, {
      kind: "get",
      key: "unit",
      value: function unit() {
        return {
          children: liveimagesSets.get(liveimages.get(this).get(sparent)).get(sli),
          active: liveimagesSets.get(liveimages.get(this).get(sparent)).get(sactive)
        };
      }
      /* props */

    }, {
      kind: "get",
      key: "parent",
      value: function parent() {
        return liveimages.get(this).get(sparent);
      }
    }, {
      kind: "get",
      key: "canvas",
      value: function canvas() {
        return liveimages.get(this).get(scanvas).get(snode);
      }
    }, {
      kind: "get",
      key: "ctx",
      value: function ctx() {
        return liveimages.get(this).get(scanvas).get(sctx);
      }
    }, {
      kind: "get",
      key: "computedStyle",
      value: function computedStyle() {
        return liveimages.get(this).get(scanvas).get(scstyle);
      }
    }, {
      kind: "get",
      key: "dimensions",
      value: function dimensions() {
        return {
          canvas: {
            width: this.canvas.width,
            height: this.canvas.height
          },
          poster: {
            start: {
              width: liveimages.get(this).get(sposter).get(sstart).get(swidth),
              height: liveimages.get(this).get(sposter).get(sstart).get(sheight)
            },
            end: {
              width: liveimages.get(this).get(sposter).get(send).get(swidth),
              height: liveimages.get(this).get(sposter).get(send).get(sheight)
            }
          },
          video: {
            width: liveimages.get(this).get(svideo).get(swidth),
            height: liveimages.get(this).get(svideo).get(sheight)
          }
        };
      }
    }, {
      kind: "get",
      key: "mediaQueryList",
      value: function mediaQueryList() {
        return liveimages.get(this).get(smql);
      }
    }, {
      kind: "get",
      key: "pointer",
      value: function pointer() {
        return liveimages.get(this).get(spointer);
      }
    }, {
      kind: "set",
      key: "pointer",
      value: function pointer(_pointer) {
        liveimages.get(this).set(spointer, _pointer);
      }
    }, {
      kind: "get",
      key: "posters",
      value: function posters() {
        return {
          start: liveimages.get(this).get(sposter).get(sstart).get(snode),
          end: liveimages.get(this).get(sposter).get(send).get(snode)
        };
      }
    }, {
      kind: "get",
      key: "video",
      value: function video() {
        return liveimages.get(this).get(svideo).get(snode);
      }
    }, {
      kind: "get",
      key: "raf",
      value: function raf() {
        return liveimages.get(this).get(sraf);
      }
    }, {
      kind: "set",
      key: "raf",
      value: function raf(v) {
        liveimages.get(this).set(sraf, v);
      }
    }, {
      kind: "get",
      key: "hrt",
      value: function hrt() {
        return liveimages.get(this).get(shrt);
      }
    }, {
      kind: "set",
      key: "hrt",
      value: function hrt(_hrt) {
        liveimages.get(this).set(shrt, _hrt);
      }
    }, {
      kind: "get",
      key: "quality",
      value: function quality() {
        return liveimages.get(this).get(squality);
      }
    }, {
      kind: "set",
      key: "quality",
      value: function quality(_quality) {
        liveimages.get(this).set(squality, _quality);
      }
    }, {
      kind: "get",
      key: "mode",
      value: function mode() {
        return liveimages.get(this).get(smode);
      }
    }, {
      kind: "set",
      key: "mode",
      value: function mode(_mode) {
        return liveimages.get(this).set(smode, _mode);
      }
    }, {
      kind: "get",
      key: "mqlAF",
      value: function mqlAF() {
        return liveimages.get(this).get(smql);
      }
    }, {
      kind: "method",
      decorators: [bound],
      key: "onmqlchange",
      value: function () {
        var _onmqlchange = _asyncToGenerator(
        /*#__PURE__*/
        regeneratorRuntime.mark(function _callee3(e) {
          var _this = this;

          return regeneratorRuntime.wrap(function _callee3$(_context3) {
            while (1) {
              switch (_context3.prev = _context3.next) {
                case 0:
                  cancelAnimationFrame(this.mqlAF);
                  liveimages.get(this).set(smql, requestAnimationFrame(
                  /*#__PURE__*/
                  function () {
                    var _ref3 = _asyncToGenerator(
                    /*#__PURE__*/
                    regeneratorRuntime.mark(function _callee2(hrt) {
                      var _filter$, video_mql, video_src, _filter$2, start_mql, start_src, _filter$3, end_mql, end_src, was;

                      return regeneratorRuntime.wrap(function _callee2$(_context2) {
                        while (1) {
                          switch (_context2.prev = _context2.next) {
                            case 0:
                              _context2.prev = 0;
                              _filter$ = _slicedToArray(_toConsumableArray(liveimages.get(_this).get(svideo).get(smql)).filter(function (_ref4) {
                                var _ref5 = _slicedToArray(_ref4, 2),
                                    mql = _ref5[0],
                                    src = _ref5[1];

                                return mql.matches;
                              })[0], 2), video_mql = _filter$[0], video_src = _filter$[1];
                              _filter$2 = _slicedToArray(_toConsumableArray(liveimages.get(_this).get(sposter).get(sstart).get(smql)).filter(function (_ref6) {
                                var _ref7 = _slicedToArray(_ref6, 2),
                                    mql = _ref7[0],
                                    src = _ref7[1];

                                return mql.matches;
                              })[0], 2), start_mql = _filter$2[0], start_src = _filter$2[1];
                              _filter$3 = _slicedToArray(_toConsumableArray(liveimages.get(_this).get(sposter).get(send).get(smql)).filter(function (_ref8) {
                                var _ref9 = _slicedToArray(_ref8, 2),
                                    mql = _ref9[0],
                                    src = _ref9[1];

                                return mql.matches;
                              })[0], 2), end_mql = _filter$3[0], end_src = _filter$3[1];
                              was = _this.pointer;
                              if (!_this.video.paused) was = "end", _this.video.pause();
                              _this.save = null; // invalidate cache

                              _context2.next = 9;
                              return _this.onposterstartsrcchange({
                                src: start_src
                              });

                            case 9:
                              _context2.next = 11;
                              return _this.onposterendsrcchange({
                                src: end_src
                              });

                            case 11:
                              _this.toPosterFrame(was);

                              _this.onvideosrcchange({
                                src: video_src
                              });

                              _context2.next = 18;
                              break;

                            case 15:
                              _context2.prev = 15;
                              _context2.t0 = _context2["catch"](0);
                              requestAnimationFrame(_this.onmqlchange);

                            case 18:
                            case "end":
                              return _context2.stop();
                          }
                        }
                      }, _callee2, null, [[0, 15]]);
                    }));

                    return function (_x2) {
                      return _ref3.apply(this, arguments);
                    };
                  }()));

                case 2:
                case "end":
                  return _context3.stop();
              }
            }
          }, _callee3, this);
        }));

        function onmqlchange(_x) {
          return _onmqlchange.apply(this, arguments);
        }

        return onmqlchange;
      }()
    }, {
      kind: "method",
      key: "ready",
      value: function () {
        var _ready = _asyncToGenerator(
        /*#__PURE__*/
        regeneratorRuntime.mark(function _callee7() {
          var _this2 = this;

          var _ref10, _ref11, video, start, end;

          return regeneratorRuntime.wrap(function _callee7$(_context7) {
            while (1) {
              switch (_context7.prev = _context7.next) {
                case 0:
                  _context7.next = 2;
                  return Promise.all([// media queries
                  //video
                  new Promise(
                  /*#__PURE__*/
                  function () {
                    var _ref12 = _asyncToGenerator(
                    /*#__PURE__*/
                    regeneratorRuntime.mark(function _callee4(resolve) {
                      var src;
                      return regeneratorRuntime.wrap(function _callee4$(_context4) {
                        while (1) {
                          switch (_context4.prev = _context4.next) {
                            case 0:
                              src = liveimages.get(_this2).get(svideo).get(ssources).reduce(function (acc, _ref13) {
                                var _ref14 = _slicedToArray(_ref13, 2),
                                    mq = _ref14[0],
                                    src = _ref14[1];

                                var mql = matchMedia(mq);
                                liveimages.get(_this2).get(svideo).get(smql).add([mql, src]);
                                mql.addListener(_this2.onmqlchange);
                                return mql.matches ? src : acc;
                              }, "");
                              resolve(src);

                            case 2:
                            case "end":
                              return _context4.stop();
                          }
                        }
                      }, _callee4);
                    }));

                    return function (_x3) {
                      return _ref12.apply(this, arguments);
                    };
                  }()) // poster start
                  , new Promise(
                  /*#__PURE__*/
                  function () {
                    var _ref15 = _asyncToGenerator(
                    /*#__PURE__*/
                    regeneratorRuntime.mark(function _callee5(resolve) {
                      var src;
                      return regeneratorRuntime.wrap(function _callee5$(_context5) {
                        while (1) {
                          switch (_context5.prev = _context5.next) {
                            case 0:
                              src = liveimages.get(_this2).get(sposter).get(sstart).get(ssources).reduce(function (acc, _ref16) {
                                var _ref17 = _slicedToArray(_ref16, 2),
                                    mq = _ref17[0],
                                    src = _ref17[1];

                                var mql = matchMedia(mq);
                                liveimages.get(_this2).get(sposter).get(sstart).get(smql).add([mql, src]);
                                mql.addListener(_this2.onmqlchange);
                                return mql.matches ? src : acc;
                              }, "");
                              resolve(src);

                            case 2:
                            case "end":
                              return _context5.stop();
                          }
                        }
                      }, _callee5);
                    }));

                    return function (_x4) {
                      return _ref15.apply(this, arguments);
                    };
                  }()) // poster end
                  , new Promise(
                  /*#__PURE__*/
                  function () {
                    var _ref18 = _asyncToGenerator(
                    /*#__PURE__*/
                    regeneratorRuntime.mark(function _callee6(resolve) {
                      var src;
                      return regeneratorRuntime.wrap(function _callee6$(_context6) {
                        while (1) {
                          switch (_context6.prev = _context6.next) {
                            case 0:
                              src = liveimages.get(_this2).get(sposter).get(send).get(ssources).reduce(function (acc, _ref19) {
                                var _ref20 = _slicedToArray(_ref19, 2),
                                    mq = _ref20[0],
                                    src = _ref20[1];

                                var mql = matchMedia(mq);
                                liveimages.get(_this2).get(sposter).get(send).get(smql).add([mql, src]);
                                mql.addListener(_this2.onmqlchange);
                                return mql.matches ? src : acc;
                              }, "");
                              resolve(src);

                            case 2:
                            case "end":
                              return _context6.stop();
                          }
                        }
                      }, _callee6);
                    }));

                    return function (_x5) {
                      return _ref18.apply(this, arguments);
                    };
                  }())]);

                case 2:
                  _ref10 = _context7.sent;
                  _ref11 = _slicedToArray(_ref10, 3);
                  video = _ref11[0];
                  start = _ref11[1];
                  end = _ref11[2];
                  _context7.next = 9;
                  return this.onposterstartsrcchange({
                    src: start
                  });

                case 9:
                  _context7.next = 11;
                  return this.onposterendsrcchange({
                    src: end
                  });

                case 11:
                  this.toPosterFrame("start");
                  _context7.next = 14;
                  return this.onvideosrcchange({
                    src: video
                  });

                case 14:
                  window.addEventListener("resize", this.onresize);
                  window.addEventListener("orientationchange", this.onresize);
                  return _context7.abrupt("return", this);

                case 17:
                case "end":
                  return _context7.stop();
              }
            }
          }, _callee7, this);
        }));

        function ready() {
          return _ready.apply(this, arguments);
        }

        return ready;
      }()
    }, {
      kind: "method",
      decorators: [bound],
      key: "onresize",
      value: function onresize(e) {
        this.resize(true);
      }
    }, {
      kind: "method",
      decorators: [bound],
      key: "onvideosrcchange",
      value: function () {
        var _onvideosrcchange = _asyncToGenerator(
        /*#__PURE__*/
        regeneratorRuntime.mark(function _callee9(_ref21) {
          var _this3 = this;

          var src, allowed;
          return regeneratorRuntime.wrap(function _callee9$(_context9) {
            while (1) {
              switch (_context9.prev = _context9.next) {
                case 0:
                  src = _ref21.src;
                  _context9.next = 3;
                  return canPlayWithoutAction;

                case 3:
                  allowed = _context9.sent;
                  _context9.next = 6;
                  return new Promise(function (resolve) {
                    var video = document.createElement("video");
                    liveimages.get(_this3).get(svideo).set(snode, video);
                    video.muted = true;
                    video.autoplay = false;
                    video.loop = false;
                    video.setAttribute("playsinline", true);
                    video.setAttribute("webkit-playsinline", true);
                    video.setAttribute("preload", "auto");
                    video.src = src;

                    var oncanplay = function oncanplay(e) {
                      _this3.video.removeEventListener("loadedmetadata", oncanplay);

                      _this3.video.removeEventListener("canplay", oncanplay);

                      requestAnimationFrame(resolve);
                    };

                    var onallowed = function onallowed() {
                      _this3.video.addEventListener("loadedmetadata", oncanplay);

                      _this3.video.addEventListener("canplay", oncanplay);
                    };

                    if (allowed) onallowed();else {
                      var onuseraction =
                      /*#__PURE__*/
                      function () {
                        var _ref22 = _asyncToGenerator(
                        /*#__PURE__*/
                        regeneratorRuntime.mark(function _callee8(e) {
                          return regeneratorRuntime.wrap(function _callee8$(_context8) {
                            while (1) {
                              switch (_context8.prev = _context8.next) {
                                case 0:
                                  _this3.parent.removeEventListener("touchstart", onuseraction);

                                  _this3.parent.removeEventListener("touchmove", onuseraction);

                                  _this3.parent.removeEventListener("mouseover", onuseraction);

                                  _this3.parent.removeEventListener("mousemove", onuseraction);

                                  _context8.prev = 4;
                                  _context8.next = 7;
                                  return _this3.video.play();

                                case 7:
                                  _this3.video.pause();

                                  video.currentTime = 0;
                                  requestAnimationFrame(resolve);
                                  _context8.next = 15;
                                  break;

                                case 12:
                                  _context8.prev = 12;
                                  _context8.t0 = _context8["catch"](4);
                                  console.warn("video::play not allowed...");

                                case 15:
                                case "end":
                                  return _context8.stop();
                              }
                            }
                          }, _callee8, null, [[4, 12]]);
                        }));

                        return function onuseraction(_x7) {
                          return _ref22.apply(this, arguments);
                        };
                      }();

                      _this3.parent.addEventListener("touchstart", onuseraction);

                      _this3.parent.addEventListener("touchmove", onuseraction);

                      _this3.parent.addEventListener("mouseover", onuseraction);

                      _this3.parent.addEventListener("mousemove", onuseraction);
                    }

                    _this3.video.setAttribute("src", src);
                  });

                case 6:
                  this.video.setAttribute("style", LiveImage.cssTextForIntrinsicDimensions);
                  document.body.appendChild(this.video);
                  liveimages.get(this).get(svideo).set(swidth, this.video.clientWidth);
                  liveimages.get(this).get(svideo).set(sheight, this.video.clientHeight);
                  document.body.removeChild(this.video);
                  this.video.removeAttribute("style");

                case 12:
                case "end":
                  return _context9.stop();
              }
            }
          }, _callee9, this);
        }));

        function onvideosrcchange(_x6) {
          return _onvideosrcchange.apply(this, arguments);
        }

        return onvideosrcchange;
      }()
    }, {
      kind: "method",
      decorators: [bound],
      key: "onposterstartsrcchange",
      value: function () {
        var _onposterstartsrcchange = _asyncToGenerator(
        /*#__PURE__*/
        regeneratorRuntime.mark(function _callee10(_ref23) {
          var _this4 = this;

          var src;
          return regeneratorRuntime.wrap(function _callee10$(_context10) {
            while (1) {
              switch (_context10.prev = _context10.next) {
                case 0:
                  src = _ref23.src;
                  _context10.next = 3;
                  return new Promise(function (resolve) {
                    _this4.posters.start.removeAttribute("src");

                    var onload = function onload(e) {
                      _this4.posters.start.removeEventListener("load", onload);

                      resolve();
                    };

                    _this4.posters.start.addEventListener("load", onload);

                    _this4.posters.start.setAttribute("src", src);
                  });

                case 3:
                  this.posters.start.setAttribute("style", LiveImage.cssTextForIntrinsicDimensions);
                  document.body.appendChild(this.posters.start);
                  this.canvas.width = this.posters.start.clientWidth;
                  this.canvas.height = this.posters.start.clientHeight;
                  liveimages.get(this).get(sposter).get(sstart).set(swidth, this.posters.start.clientWidth);
                  liveimages.get(this).get(sposter).get(sstart).set(sheight, this.posters.start.clientHeight);
                  document.body.removeChild(this.posters.start);
                  this.posters.start.removeAttribute("style");

                case 11:
                case "end":
                  return _context10.stop();
              }
            }
          }, _callee10, this);
        }));

        function onposterstartsrcchange(_x8) {
          return _onposterstartsrcchange.apply(this, arguments);
        }

        return onposterstartsrcchange;
      }()
    }, {
      kind: "method",
      decorators: [bound],
      key: "onposterendsrcchange",
      value: function () {
        var _onposterendsrcchange = _asyncToGenerator(
        /*#__PURE__*/
        regeneratorRuntime.mark(function _callee11(_ref24) {
          var _this5 = this;

          var src;
          return regeneratorRuntime.wrap(function _callee11$(_context11) {
            while (1) {
              switch (_context11.prev = _context11.next) {
                case 0:
                  src = _ref24.src;
                  _context11.next = 3;
                  return new Promise(function (resolve) {
                    _this5.posters.end.removeAttribute("src");

                    var onload = function onload(e) {
                      _this5.posters.end.removeEventListener("load", onload);

                      resolve();
                    };

                    _this5.posters.end.addEventListener("load", onload);

                    _this5.posters.end.setAttribute("src", src);
                  });

                case 3:
                  this.posters.end.setAttribute("style", LiveImage.cssTextForIntrinsicDimensions);
                  document.body.appendChild(this.posters.end);
                  liveimages.get(this).get(sposter).get(send).set(swidth, this.posters.end.clientWidth);
                  liveimages.get(this).get(sposter).get(send).set(sheight, this.posters.end.clientHeight);
                  document.body.removeChild(this.posters.end);
                  this.posters.end.removeAttribute("style");

                case 9:
                case "end":
                  return _context11.stop();
              }
            }
          }, _callee11, this);
        }));

        function onposterendsrcchange(_x9) {
          return _onposterendsrcchange.apply(this, arguments);
        }

        return onposterendsrcchange;
      }()
      /* DRAWING/FRAMES */

    }, {
      kind: "method",
      key: "cancelAF",
      value: function cancelAF() {
        var to = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "end";
        cancelAnimationFrame(this.raf);
        this.raf = null; // will be used to know if it's a manual pause or not

        if (!this.video.paused) this.video.pause();
        if (this.pointer !== to) this.toPosterFrame(to);
      }
    }, {
      kind: "method",
      key: "requestAF",
      value: function () {
        var _requestAF = _asyncToGenerator(
        /*#__PURE__*/
        regeneratorRuntime.mark(function _callee12() {
          var _this6 = this;

          var from,
              _args12 = arguments;
          return regeneratorRuntime.wrap(function _callee12$(_context12) {
            while (1) {
              switch (_context12.prev = _context12.next) {
                case 0:
                  from = _args12.length > 0 && _args12[0] !== undefined ? _args12[0] : 0;
                  this.raf = requestAnimationFrame(function (hrt) {
                    // if ( hrt - this.hrt < 16.67 )
                    // return this.raf = requestAnimationFrame(() => this.requestAF(from))
                    _this6.hrt = hrt;

                    var to = _this6.frameHandler(hrt); // console.log(from, "=>", to)
                    // console.log("paused?", this.video.paused)


                    if (to < 1 && !_this6.video.paused) _this6.requestAF(to);else if (from === to && to === 1) {
                      if (!_this6.video.paused) _this6.video.pause();
                    }
                  });

                case 2:
                case "end":
                  return _context12.stop();
              }
            }
          }, _callee12, this);
        }));

        function requestAF() {
          return _requestAF.apply(this, arguments);
        }

        return requestAF;
      }()
    }, {
      kind: "method",
      key: "frameHandler",
      value: function frameHandler(hrt) {
        var _this7 = this;

        this.save = function () {
          var _this7$ctx, _this7$ctx2;

          if (debug) console.log("frameHandler");
          var _this7$dimensions = _this7.dimensions,
              _this7$dimensions$can = _this7$dimensions.canvas,
              ncw = _this7$dimensions$can.width,
              nch = _this7$dimensions$can.height,
              _this7$dimensions$vid = _this7$dimensions.video,
              nvw = _this7$dimensions$vid.width,
              nvh = _this7$dimensions$vid.height;
          var progress = _this7.video.currentTime / _this7.video.duration;
          if (_this7.mode === "contain") (_this7$ctx = _this7.ctx).drawImage.apply(_this7$ctx, _toConsumableArray(LiveImage.scale.contain(_this7.video, nvw, nvh, ncw, nch)));else (_this7$ctx2 = _this7.ctx).drawImage.apply(_this7$ctx2, _toConsumableArray(LiveImage.scale.cover(_this7.video, nvw, nvh, ncw, nch)));
          return progress;
        };

        return this.save();
      }
    }, {
      kind: "method",
      key: "play",
      value: function () {
        var _play = _asyncToGenerator(
        /*#__PURE__*/
        regeneratorRuntime.mark(function _callee13() {
          var _this8 = this;

          var onplaying, onend;
          return regeneratorRuntime.wrap(function _callee13$(_context13) {
            while (1) {
              switch (_context13.prev = _context13.next) {
                case 0:
                  if (this.video.paused) {
                    _context13.next = 2;
                    break;
                  }

                  return _context13.abrupt("return", Promise.resolve());

                case 2:
                  this.video.currentTime = 0;

                  onplaying = function onplaying(e) {
                    _this8.video.removeEventListener("playing", onplaying);

                    _this8.requestAF();
                  };

                  onend = function onend(e) {
                    if (_this8.raf) _this8.cancelAF();

                    _this8.video.removeEventListener("playing", onplaying);

                    _this8.video.removeEventListener("pause", onend);

                    _this8.video.removeEventListener("ended", onend);

                    _this8.video.removeEventListener("error", onend);

                    _this8.video.removeEventListener("stalled", onend);
                  };

                  this.video.addEventListener("playing", onplaying);
                  this.video.addEventListener("pause", onend);
                  this.video.addEventListener("ended", onend);
                  this.video.addEventListener("error", onend);
                  this.video.addEventListener("stalled", onend);
                  _context13.prev = 10;
                  this.resize(false);
                  this.pointer = "video";
                  return _context13.abrupt("return", this.video.play());

                case 16:
                  _context13.prev = 16;
                  _context13.t0 = _context13["catch"](10);
                  return _context13.abrupt("return", this.cancelAF());

                case 19:
                case "end":
                  return _context13.stop();
              }
            }
          }, _callee13, this, [[10, 16]]);
        }));

        function play() {
          return _play.apply(this, arguments);
        }

        return play;
      }()
    }, {
      kind: "method",
      key: "reset",
      value: function () {
        var _reset = _asyncToGenerator(
        /*#__PURE__*/
        regeneratorRuntime.mark(function _callee14() {
          return regeneratorRuntime.wrap(function _callee14$(_context14) {
            while (1) {
              switch (_context14.prev = _context14.next) {
                case 0:
                  return _context14.abrupt("return", this.cancelAF("start"));

                case 1:
                case "end":
                  return _context14.stop();
              }
            }
          }, _callee14, this);
        }));

        function reset() {
          return _reset.apply(this, arguments);
        }

        return reset;
      }()
    }, {
      kind: "method",
      key: "resize",
      value: function resize() {
        var redraw = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
        // TODO move to LICARD.js
        // this.canvas.style.removeProperty("height")
        var _this$computedStyle = this.computedStyle,
            width = _this$computedStyle.width,
            height = _this$computedStyle.height;
        var _ref25 = [Math.ceil(parseFloat(width)), Math.ceil(parseFloat(height))],
            nwidth = _ref25[0],
            nheight = _ref25[1];
        this.canvas.width = nwidth * this.quality;
        this.canvas.height = nheight * this.quality; // TODO move to LICARD.js
        // this.canvas.style.setProperty("height", nheight+"px")

        if (this.save) this.save();
      }
    }, {
      kind: "get",
      key: "save",
      value: function save() {
        return liveimages.get(this).get(ssaved);
      }
    }, {
      kind: "set",
      key: "save",
      value: function save(fn) {
        liveimages.get(this).set(ssaved, fn);
      }
    }, {
      kind: "method",
      key: "toPosterFrame",
      value: function toPosterFrame(which) {
        var _this9 = this;

        this.save = function () {
          var _this9$ctx, _this9$ctx2;

          var _this9$dimensions = _this9.dimensions,
              _this9$dimensions$can = _this9$dimensions.canvas,
              ncw = _this9$dimensions$can.width,
              nch = _this9$dimensions$can.height,
              _this9$dimensions$pos = _this9$dimensions.poster[which],
              npw = _this9$dimensions$pos.width,
              nph = _this9$dimensions$pos.height;
          if (_this9.mode === "contain") (_this9$ctx = _this9.ctx).drawImage.apply(_this9$ctx, _toConsumableArray(LiveImage.scale.contain(_this9.posters[which], npw, nph, ncw, nch)));else (_this9$ctx2 = _this9.ctx).drawImage.apply(_this9$ctx2, _toConsumableArray(LiveImage.scale.cover(_this9.posters[which], npw, nph, ncw, nch))); // for debug, useful
          // if ( which === "end" ){
          //     this.ctx.fillStyle = "red"
          // } else {
          //     this.ctx.fillStyle = "blue"
          // }
          // this.ctx.fillRect(0, 0, ncw, nch)

          _this9.pointer = which; // this.ctx.save()
          // this.dispatchEvent(createEvent(LiveImage.events.frame, {
          //     ctx: this.ctx, width:ncw, height: nch, poster: true, [which]: true
          // }))
          // this.ctx.restore()
        };

        this.resize(false);
        return this.save();
      }
    }]
  };
});



/***/ })
/******/ ])});;
//# sourceMappingURL=liveimage.js.map