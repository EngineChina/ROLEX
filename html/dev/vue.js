    var app = new Vue({
        el: '#app',
        data: {
            message: 'Hello Vue!',
            isMenu: false
        },
        methods: {
            handleMenuClose() {
                this.isMenu = !this.isMenu
            }
        }
    })